package com.hector.android.notifications;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.hector.android.R;
import com.hector.android.activities.ActivitiesActivity;
import com.hector.android.application.Hector;

/**
 * Created by Arjun on 9/7/15.
 */
public class NotificationService extends IntentService {
    public static final int NOTIFICATION_ID = 888;
    public static final String IS_NEW_NOTIFICATION = "IS_NEW_NOTIFICATION";
    private static final String TAG = "NotificationService";
    private static final int REQUEST_CODE = 3322;
    public static final String NOTIFICATION_ACTION = "NOTIFICATION_ACTION";
    public static final int ACTION_SHOW = 1;
    public static final int ACTION_CLEAR = 2;
    public static final String NOTIFICATION_ACTOR_ID = "NOTIFICATION_ACTOR_ID";
    public static final String NOTIFICATION_ACTOR_NAME = "NOTIFICATION_ACTOR_NAME";
    private NotificationManager mNotificationManager;
    private int requestCode = 987654;
    private Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public NotificationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int action = intent.getIntExtra(NOTIFICATION_ACTION, ACTION_CLEAR);
        switch (action) {
            case ACTION_SHOW:
                if (!Hector.preferToShowNotification) {
                    return;
                }
                String actorId = intent.getStringExtra(NOTIFICATION_ACTOR_ID);
                String actorName = intent.getStringExtra(NOTIFICATION_ACTOR_NAME);
                mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setAutoCancel(true)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentTitle("Hector New activity")
                        .setContentText("New activity received from " + actorName)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setSound(alarmSound);
                Intent notificationIntent = new Intent(this, ActivitiesActivity.class);
                notificationIntent.putExtra("userId", actorId);
                notificationIntent.putExtra("userName", actorName);
                TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
                taskStackBuilder.addParentStack(ActivitiesActivity.class);
                taskStackBuilder.addNextIntent(notificationIntent);
                PendingIntent contentIntent = taskStackBuilder.getPendingIntent(requestCode, PendingIntent.FLAG_CANCEL_CURRENT);
                builder.setContentIntent(contentIntent);
                mNotificationManager.notify(NOTIFICATION_ID, builder.build());
                break;
            case ACTION_CLEAR:
                clearPreviousNotifications();
                break;
        }
    }

    private void clearPreviousNotifications() {
        mNotificationManager.cancel(NotificationService.NOTIFICATION_ID);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "In onDestroy");
    }
}
