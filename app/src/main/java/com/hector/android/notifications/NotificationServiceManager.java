package com.hector.android.notifications;

import android.content.Intent;

import com.hector.android.application.Hector;

/**
 * Created by arjun on 12/2/15.
 */
public class NotificationServiceManager {
    public static void showNewActivityNotification(String actorId, String actorName){
        Intent notificationIntent = new Intent(Hector.applicationContext, NotificationService.class);
        notificationIntent.putExtra(NotificationService.NOTIFICATION_ACTION, NotificationService.ACTION_SHOW);
        notificationIntent.putExtra(NotificationService.NOTIFICATION_ACTOR_ID, actorId);
        notificationIntent.putExtra(NotificationService.NOTIFICATION_ACTOR_NAME, actorName);
        Hector.applicationContext.startService(notificationIntent);
    }

    public static void clearAllNotification(){
        Intent notificationIntent = new Intent(Hector.applicationContext, NotificationService.class);
        notificationIntent.putExtra(NotificationService.NOTIFICATION_ACTION, NotificationService.ACTION_CLEAR);
        Hector.applicationContext.startService(notificationIntent);
    }
}
