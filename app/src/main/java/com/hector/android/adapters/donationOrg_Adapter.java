package com.hector.android.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.hector.android.R;

import java.util.List;

/**
 * Created by amal on 22/03/16.
 */
public class donationOrg_Adapter extends BaseAdapter {

    List<Integer> images;
    private Activity activity;
    private LayoutInflater inflater;

    public donationOrg_Adapter(Activity activity, List<Integer> images) {
        this.activity = activity;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return images.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = new ViewHolder();

        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.donation_column, null);
            holder.image = (ImageView) convertView.findViewById(R.id.donation_org_image);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();


        holder.image.setImageResource(images.get(position));


        return convertView;
    }

    public static class ViewHolder {
        ImageView image;
    }
}
