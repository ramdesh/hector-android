package com.hector.android.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hector.android.R;
import com.hector.android.application.Hector;
import com.hector.android.application.Utils;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.libs.recycleriewanimator.RecyclerViewScrollAnimator;
import com.hector.android.pojo.responses.patientactivities.Activity;

import java.util.ArrayList;

/**
 * Created by arjun on 11/23/15.
 */
public class ActivitiesAdapter extends RecyclerView.Adapter<ActivitiesAdapter.ActivitiesViewHolder> {
    public static final int VIEW_TYPE_DEFAULT = 0;
    public static final int VIEW_TYPE_END_OF_LIST = 1;
    public static final int VIEW_TYPE_LOADING = 2;
    public static final int VIEW_TYPE_SECTION = 3;
    private final Context context;
    private final int VIEW_TYPE_COUNT = 2;
    private final ArrayList<Activity> activities;
    private final LayoutInflater layoutInflater;
    private RecyclerViewScrollAnimator recyclerViewScrollAnimator;

    public ActivitiesAdapter(Context context, ArrayList<Activity> activities, RecyclerView recyclerView) {
        this.context = context;
        this.activities = activities;
        layoutInflater = LayoutInflater.from(context);
        recyclerViewScrollAnimator = new RecyclerViewScrollAnimator(recyclerView);
    }

    @Override
    public ActivitiesAdapter.ActivitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ActivitiesViewHolder.createViewHolder(parent, viewType, layoutInflater);
    }

    @Override
    public int getItemViewType(int position) {
        return activities.get(position).viewType;
    }

    @Override
    public void onBindViewHolder(ActivitiesAdapter.ActivitiesViewHolder holder, int position) {
        Activity activity = activities.get(position);
        holder.bindData(context, activity, position, recyclerViewScrollAnimator);
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    static class ListContentLayoutViewHolder extends ActivitiesViewHolder {

        public final TypeFaceTextView timeStampView;
        public final TypeFaceTextView contentHeadingView;
        public final TypeFaceTextView contentTextView;
        public final CardView contentTextBgCardView;
        public final View activityTypeIndicator;

        public ListContentLayoutViewHolder(View itemView) {
            super(itemView);
            timeStampView = (TypeFaceTextView) itemView.findViewById(R.id.timestamp_text_view);
            contentHeadingView = (TypeFaceTextView) itemView.findViewById(R.id.content_heading_text_view);
            contentTextView = (TypeFaceTextView) itemView.findViewById(R.id.content_text_view);
            contentTextBgCardView = (CardView) itemView.findViewById(R.id.layout_card_view);
            activityTypeIndicator = itemView.findViewById(R.id.activity_type_indicator_bar);
        }

        @Override
        void bindData(Context context, Activity activity, int position, RecyclerViewScrollAnimator recyclerViewScrollAnimator) {
            if (position == 0) {
                itemView.setPadding(0, Hector.toolBarLayoutHeight * 2, 0, 0);
            } else {
                itemView.setPadding(0, 0, 0, 0);
            }
            timeStampView.setText(Utils.getTimeFormatB(activity.dateTime));  // activity.createdAt
            String title = "";

            contentHeadingView.setText(title);
            contentHeadingView.setCompoundDrawablesWithIntrinsicBounds(Utils.activityTypeIconMap.get(activity.activityLevel), 0, 0, 0);
            activityTypeIndicator.setBackgroundResource(Utils.activityTypeColorMap.get(activity.activityLevel));
            String message = "";

            message = activity.message;
            message = message.isEmpty() ? context.getResources().getString(R.string.no_details_found) : message;
            contentTextView.setText(message);
            recyclerViewScrollAnimator.onAnimateViewHolder(contentTextBgCardView, position);
        }
    }

    static class ListEndLayoutViewHolder extends ActivitiesViewHolder {

        public ListEndLayoutViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        void bindData(Context context, Activity activity, int position, RecyclerViewScrollAnimator recyclerViewScrollAnimator) {

        }
    }

    static class LoadingLayoutViewHolder extends ActivitiesViewHolder {
        public LoadingLayoutViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        void bindData(Context context, Activity activity, int position, RecyclerViewScrollAnimator recyclerViewScrollAnimator) {

        }
    }

    static class SectionLayoutViewHolder extends ActivitiesViewHolder {
        private final TypeFaceTextView timeStampView;

        public SectionLayoutViewHolder(View itemView) {
            super(itemView);
            timeStampView = (TypeFaceTextView) itemView.findViewById(R.id.timestamp_section_text_view);
        }

        @Override
        void bindData(Context context, Activity activity, int position, RecyclerViewScrollAnimator recyclerViewScrollAnimator) {
            if (position == 0) {
                itemView.setPadding(0, Hector.toolBarLayoutHeight * 2, 0, 0);
            } else {
                itemView.setPadding(0, 0, 0, 0);
            }
            timeStampView.setText(activity.sectionDetails);
        }
    }

    static abstract class ActivitiesViewHolder extends RecyclerView.ViewHolder {
        public ActivitiesViewHolder(View itemView) {
            super(itemView);
        }

        public static ActivitiesViewHolder createViewHolder(ViewGroup parent, int viewType, LayoutInflater layoutInflater) {
            View view;
            switch (viewType) {
                default:
                case VIEW_TYPE_DEFAULT:
                    view = layoutInflater.inflate(R.layout.activities_item_layout, parent, false);
                    return new ListContentLayoutViewHolder(view);

                case VIEW_TYPE_END_OF_LIST:
                    view = layoutInflater.inflate(R.layout.activities_last_item_layout, parent, false);
                    ;
                    return new ListEndLayoutViewHolder(view);

                case VIEW_TYPE_LOADING:
                    view = layoutInflater.inflate(R.layout.activities_loading_item_layout, parent, false);
                    ;
                    return new LoadingLayoutViewHolder(view);

                case VIEW_TYPE_SECTION:
                    view = layoutInflater.inflate(R.layout.activities_section_view_layout, parent, false);
                    ;
                    return new SectionLayoutViewHolder(view);
            }
        }

        abstract void bindData(Context context, Activity activity, int position, RecyclerViewScrollAnimator recyclerViewScrollAnimator);
    }

}
