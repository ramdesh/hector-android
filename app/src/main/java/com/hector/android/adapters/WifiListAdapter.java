package com.hector.android.adapters;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hector.android.R;
import com.hector.android.libs.TypeFaceTextView;

import java.util.ArrayList;

/**
 * Created by arjun on 11/20/15.
 */
public class WifiListAdapter extends RecyclerView.Adapter<WifiListAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<ScanResult> wifiResults;
    private ItemClickListener itemClickListener;

    public WifiListAdapter(Context context, ArrayList<ScanResult> scanResults) {
        this.context = context;
        this.wifiResults = scanResults;
    }

    @Override
    public WifiListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.wifi_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WifiListAdapter.ViewHolder holder, final int position) {
        ScanResult scanResult = wifiResults.get(position);
        String wifiName = scanResult.SSID;
        if (wifiName.isEmpty()) {
            wifiName = "Unknown";
        }
        holder.wifiNameView.setText(wifiName);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return wifiResults.size();
    }

    public void setOnItemClickListener(ItemClickListener onItemClickListener) {
        this.itemClickListener = onItemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TypeFaceTextView wifiNameView;
        public View itemView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            wifiNameView = (TypeFaceTextView) itemView.findViewById(R.id.wifi_name_view);
        }
    }
}
