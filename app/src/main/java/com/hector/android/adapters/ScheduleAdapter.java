package com.hector.android.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hector.android.R;
import com.hector.android.application.Utils;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.responses.patientschedules.dailyAdherence;

import java.util.ArrayList;

/**
 * Created by arjun on 11/26/15.
 */
public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {

    private static final int MEDICATIONS_PER_DAY = 4;
    private final Context context;
    private final ArrayList<dailyAdherence> schedules;
    private final String[] timesOfDay = {"MORNING", "NOON", "EVENING", "NIGHT"};
    private final String[] statuses = {"scheduled", "consumed", "missed"};
    private final int[] timeImages = {R.drawable.morning, R.drawable.noon, R.drawable.evening, R.drawable.night};

    public ScheduleAdapter(Context context, ArrayList<dailyAdherence> schedules) {
        this.context = context;
        this.schedules = schedules;
    }

    @Override
    public ScheduleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.schedule_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ScheduleAdapter.ViewHolder holder, int position) {

        dailyAdherence schedule = schedules.get(position);
        holder.headerView.setText(timesOfDay[position]);
        if (schedule.medications != null && schedule.medications.size() > 0) {
            String medicationText = "";
            for (dailyAdherence.Medication medication : schedule.medications) {
                medicationText = medicationText + medication.quantity + " x " + medication.drug + "\n";
            }
            holder.contentView.setText(medicationText);
            holder.headerView.setText(timesOfDay[position] + "  (" + Utils.getTimeFormatC(schedule.actualTime) + ")");
            // to change the background color if status is missed
            if (schedule.adherenceLevel.equals(statuses[2])) {
                holder.headerView.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.red_dot), null);
                //holder.scheduleLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.missed));
            } else if (schedule.status.equals(statuses[1])) {
                holder.headerView.setPaintFlags(holder.headerView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.headerView.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.green_dot), null);
            } else {
                holder.headerView.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.green_dot), null);
            }
        } else {
            holder.contentView.setText(context.getResources().getString(R.string.no_medication));
//
            holder.headerView.setCompoundDrawablesWithIntrinsicBounds(null, null, context.getResources().getDrawable(R.drawable.red_dot), null);
        }
        holder.scheduleTimeIv.setImageDrawable(context.getResources().getDrawable(timeImages[position]));
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView scheduleTimeIv;
        public TypeFaceTextView headerView;
        public TypeFaceTextView contentView;
        public LinearLayout scheduleLinearLayout;

        //        View sideColorBar;
        public ViewHolder(View itemView) {
            super(itemView);
            headerView = (TypeFaceTextView) itemView.findViewById(R.id.schedule_content_heading_text_view);
            contentView = (TypeFaceTextView) itemView.findViewById(R.id.schedule_content_text_view);
            scheduleTimeIv = (ImageView) itemView.findViewById(R.id.schedule_time_image_view);
            scheduleLinearLayout = (LinearLayout) itemView.findViewById(R.id.scheduleLinearLayout);
        }
    }
}
