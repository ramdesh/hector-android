package com.hector.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hector.android.R;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.responses.pateintdevicedetails.Device;

import java.util.ArrayList;

/**
 * Created by arjun on 12/12/15.
 */
public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Device> devices;
    private LayoutInflater layoutInflater;

    public DevicesAdapter(Context context, ArrayList<Device> devices) {
        this.context = context;
        this.devices = devices;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public DevicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.device_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DevicesAdapter.ViewHolder holder, int position) {
        Device device = devices.get(position);

        if (device.isActive) {
            holder.deviceStatusView.setText(context.getResources().getString(R.string.connected));
            holder.deviceModeView.setText(device.isUsingWifi ? "WIFI" : "GSM");
        } else {
            holder.deviceStatusView.setText(context.getResources().getString(R.string.disconnected));
            holder.deviceModeView.setText("NA");
        }

    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TypeFaceTextView deviceStatusView;
        TypeFaceTextView deviceModeView;

        public ViewHolder(View itemView) {
            super(itemView);
            deviceStatusView = (TypeFaceTextView) itemView.findViewById(R.id.device_online_status);
            deviceModeView = (TypeFaceTextView) itemView.findViewById(R.id.device_mode);
        }
    }
}
