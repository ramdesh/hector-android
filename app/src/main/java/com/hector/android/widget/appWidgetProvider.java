package com.hector.android.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.hector.android.R;
import com.hector.android.activities.KarmaActivity;
import com.hector.android.activities.LauncherActivity;
import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.karmadetails.karmaResponse;
import com.hector.android.pojo.responses.weatherResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by amal on 23/03/16.
 */
public class appWidgetProvider extends AppWidgetProvider {

    private String LOG_TAG = "appWidgetProvider";
    private RemoteViews views;
    private AppWidgetManager manager;
    private int[] widgetIds;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        final int N = appWidgetIds.length;
        manager = appWidgetManager;
        widgetIds = appWidgetIds;

        // Perform this loop procedure for each App Widget that belongs to this
        // provider
        for (int i = 0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];

            // Create an Intent to launch ExampleActivity
            Intent intent = new Intent(context, KarmaActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            // Get the layout for the App Widget
            views = new RemoteViews(context.getPackageName(), R.layout.widget);


            if ( Hector.isUserLoggedIn() && !Hector.currentPatientId.isEmpty()) {
                getHealthIndex();
                getWeather();
                views.setViewVisibility(R.id.widget_nodata, View.GONE);
                views.setViewVisibility(R.id.karma_point_text_widget, View.VISIBLE);
                views.setViewVisibility(R.id.trending_widget, View.VISIBLE);
                views.setViewVisibility(R.id.karma_points_widget, View.VISIBLE);
                views.setViewVisibility(R.id.weather_icon_widget, View.VISIBLE);
                if (SharedPreferencesManager.getStringPreference(SharedPreferencesManager.USER_TYPE, "").equals("caregiver")){
                    views.setViewVisibility(R.id.donate_widget, View.GONE);
                }else {
                    views.setViewVisibility(R.id.donate_widget,View.VISIBLE);
                    views.setOnClickPendingIntent(R.id.donate_widget, pendingIntent);
                }

            }else {

                views.setTextViewText(R.id.temperature_widget,"--");
                views.setViewVisibility(R.id.donate_widget, View.GONE);
                views.setViewVisibility(R.id.weather_icon_widget, View.GONE);
                views.setViewVisibility(R.id.karma_point_text_widget, View.GONE);
                views.setViewVisibility(R.id.trending_widget, View.GONE);
                views.setViewVisibility(R.id.karma_points_widget, View.GONE);
                views.setViewVisibility(R.id.widget_nodata, View.VISIBLE);
            }

            // Tell the AppWidgetManager to perform an update on the current app
            // widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }

    }

    private void getHealthIndex() {

        Calendar calendar = Calendar.getInstance();
        //calendar.add(Calendar.WEEK_OF_YEAR, -1);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = formatter.format(calendar.getTime());

        MyHttp.getInstance(Hector.applicationContext, Endpoints.getHealthIndex(formattedDate), MyHttp.GET, karmaResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .send(new MyHttpCallback<karmaResponse>() {
                    @Override
                    public void success(karmaResponse data) {

                        if (data.code == null) {

                            if (!data.healthIndexDatas.isEmpty()) {
                                views.setTextViewText(R.id.karma_points_widget,String.format("%.2f",data.totalKarma));
                                if (data.healthIndexDatas.get(0).trendingDirection==1){
                                    views.setImageViewResource(R.id.trending_widget,R.drawable.up_arrow);
                                }else {
                                    views.setImageViewResource(R.id.trending_widget,R.drawable.down_arrow);
                                }

                            }
                            updateWidget();
                        }
                    }

                    @Override
                    public void failure(String msg) {

                        Log.i(LOG_TAG, "sending wellness index failed");
                    }

                    @Override
                    public void onBefore() {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
    }

    private void getWeather() {

        Calendar calendar = Calendar.getInstance();
        //calendar.add(Calendar.WEEK_OF_YEAR, -1);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = formatter.format(calendar.getTime());

        MyHttp.getInstance(Hector.applicationContext, Endpoints.getWeather(), MyHttp.GET, weatherResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .send(new MyHttpCallback<weatherResponse>() {
                    @Override
                    public void success(weatherResponse data) {

                        if (data.code == null) {

                            views.setTextViewText(R.id.temperature_widget, data.temperature +"\u00B0");
                            switch (data.iconCode){
                                case 3:
                                case 4:
                                case 47:
                                case 38:
                                case 37:
                                    //thunderstorm
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.thunderstorm);
                                    break;
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 10:
                                case 18:
                                case 46:
                                case 43:
                                case 42:
                                    //sleet
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.sleet);
                                    break;
                                case 9:
                                    //drizzle
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.drizzle);
                                    break;
                                case 11:
                                case 39:
                                    //light_rain
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.light_rain);
                                    break;
                                case 12:
                                case 45:
                                case 44:
                                case 40:
                                    //rain
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.rain);
                                    break;
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                    //snow
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.snow);
                                    break;
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                    //foggy
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.foggy);
                                    break;
                                case 23:
                                    //wind
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.wind);
                                    break;
                                case 24:
                                    //cloudy_windy
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.cloudy_windy);
                                    break;
                                case 25:
                                    //frigid
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.frigid);
                                    break;
                                case 26:
                                case 27:
                                case 28:
                                    //cloudy
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.cloudy);
                                    break;
                                case 29:
                                    //partly_cloudy_night
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.partly_cloudy_night);
                                    break;
                                case 30:
                                case 34:
                                    //partly_cloudy_day
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.partly_cloudy_day);
                                    break;
                                case 31:
                                    //clear_night
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.clear_night);
                                    break;
                                case 32:
                                case 36:
                                    //clear_sun
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.clear_sun);
                                    break;
                                case 33:
                                    //cloudy_night
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.cloudy_night);
                                case 41:
                                    //sun_snow
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.sun_snow);
                                    break;
                                case 35:
                                    //hail
                                    views.setImageViewResource(R.id.weather_icon_widget,R.drawable.hail);
                                    break;

                            }

                            updateWidget();
                        }
                    }

                    @Override
                    public void failure(String msg) {

                        Log.i(LOG_TAG, "sending wellness index failed");
                    }

                    @Override
                    public void onBefore() {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
    }


    private void updateWidget() {
        for (int i = 0; i < widgetIds.length; i++) {
            int appWidgetId = widgetIds[i];
            manager.updateAppWidget(appWidgetId, views);

        }
    }

}
