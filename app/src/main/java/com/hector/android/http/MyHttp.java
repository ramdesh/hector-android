package com.hector.android.http;

import android.content.Context;
import android.graphics.Bitmap;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.HeadersCallback;
import com.koushikdutta.ion.HeadersResponse;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.koushikdutta.ion.future.ResponseFuture;

/**
 * Created by Arjun on 8/7/15.
 */
public class MyHttp<T> {
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
    public static final String HEADER_ACCESS_TOKEN = "x-user-token";
    public static final int REQUEST_OK = 200;
    public static final int GET_DEVICE_FAIL = 1040;
    public static final String HEADER_CONTENT_LENGTH = "Content-Length";
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String X_APP_ID = "x-app-id";
    public static final String AUTHORIZATION = "Authorization";
    private static final int AUTH_FAIL = 1;
    private static final int NO_ERROR = 0;
    private static final int UNEXPECTED_ERROR = 2;
    private static final int NOT_FOUND = 3;
    private static final int AUTH_FAIL_CODE = 5001;
    public final String url;
    public final String method;
    public MyHttp httpReference;
    public MyHttpCallback<T> myHttpCallback;
    public
    Context context;
    Builders.Any.B ionLoad;
    private Class responseClass;
    private Object body;
    private int errorCode;
    private String msg = "";

    public MyHttp(Context context, String url, String method, Class responseClass) {
        ionLoad = Ion.with(context).load(method, url);
        Ion.getDefault(context).configure().getResponseCache().setCaching(true);
        this.url = url;
        this.method = method;
        this.context = context;
        this.responseClass = responseClass;
    }

    public static MyHttp getInstance(Context context, String url, String method, Class responseClass) {
        return new MyHttp(context, url, method, responseClass);
    }

    public static void fetchBitmap(final Context context, final String url, final MyHttpCallback<Bitmap> callback) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                callback.onBefore();
                if (url == null || url.isEmpty() || context == null) {
                    callback.onFinish();
                    return;
                }
                Builders.Any.B load = Ion.with(context)
                        .load(url);
                try {
                    load.asBitmap()
                            .setCallback(new FutureCallback<Bitmap>() {
                                @Override
                                public void onCompleted(Exception e, Bitmap result) {
                                    if (result != null) {
                                        callback.success(result);
                                        callback.onFinish();
                                    } else {
                                        callback.failure("");
                                    }
                                }
                            });
                } catch (Exception ex) {
                    callback.failure("");
                }
            }
        }).run();
    }

    public MyHttp setDefaultHeaders() {
        ionLoad.addHeader(HEADER_CONTENT_TYPE, "application/json");
//        ionLoad.addHeader("access-token", SeeChat.loggedInUserToken);
        return this;
    }

    public MyHttp setHeaders(String key, String value) {
        ionLoad.addHeader(key, value);
        return this;
    }

    public MyHttp addJson(Object body) {
        this.body = body;
        ionLoad.setJsonPojoBody(body);
        return this;
    }

    public MyHttp send(final MyHttpCallback<T> callback) {
        httpReference = this;
        myHttpCallback = callback;
        this.errorCode = MyHttp.NO_ERROR;

        final ResponseFuture<T> jsonObjectResponseFuture = ionLoad.as(responseClass);
        setHeaderCallback(callback, jsonObjectResponseFuture);
        callback.onBefore();
        jsonObjectResponseFuture.
                setCallback(new FutureCallback<T>() {
                    @Override
                    public void onCompleted(Exception e, T result) {
                        if (e != null || MyHttp.this.errorCode != MyHttp.NO_ERROR) { //quit on errr
                            if (MyHttp.this.errorCode != MyHttp.NO_ERROR) {
                                //todo check for other headers

                            } else {
                                MyHttp.this.msg = e.getMessage() == null ? "" : e.getMessage();
                            }
                            callback.failure(MyHttp.this.msg);
                        } else {
                            callback.success(result);
                        }
                        callback.onFinish();
                    }
                });
        return this;
    }

    private void setHeaderCallback(final MyHttpCallback callback, final ResponseFuture jsonObjectResponseFuture) {
        ionLoad.onHeaders(new HeadersCallback() {
            @Override
            public void onHeaders(HeadersResponse headers) {
                if (headers.code() != 200) {
                    if (headers.code() == 401) {
                        MyHttp.this.errorCode = MyHttp.AUTH_FAIL;
                    }
                }
            }
        });
    }
}
