package com.hector.android.http;

import com.hector.android.application.Hector;

/**
 * Created by Arjun on 8/7/15.
 */
public class Endpoints {
    public static String baseUrl = "http://hector-api-services-staging.mybluemix.net/api";
    public static String authUrl = baseUrl + "/auth";
    public static String patientUrl = baseUrl + "/patients";
    public static String userUrl = baseUrl + "/users";
    public static String deviceUrl = baseUrl + "/devices";
    public static String patientDeviceUrl = patientUrl + "/devices";
    public static String changePassword = userUrl + "/changepasswd";

    public static String getPatientDetail(String patientId) {
        return patientUrl + "/" + patientId;
    }

    public static String getUserDetail(String userId) {
        return userUrl + "/" + userId;
    }

    public static String getGcmRegisterUrl(String userId) {
        return userUrl + "/" + userId + "/token";
    }

    public static String getWellnessIndexUrl(String rating) {
        return patientUrl + "/" + Hector.currentPatientId + "/wellness/" + rating;
    }

    public static String getHealthIndex(String startDate) {
        return patientUrl + "/" + Hector.currentPatientId + "/health-index?start-date=" + startDate;
    }

    public static String getWeather() {
        return patientUrl + "/" + Hector.currentPatientId + "/weather";
    }

    public static String getDeleteGcmUrl(String userId) {
        return userUrl + "/" + userId + "/token/android";
    }

    public static String getPreference(String userId) {
        return userUrl + "/" + userId + "/preferences";
    }

    public static String getAssociatedDevice(String patientId) {
        return patientUrl + "/" + patientId + "/devices";
    }

    public static String getSchedulesOn(String date) {
        return patientUrl + "/" + Hector.currentPatientId + "/adherence?start-date=" + date + "&end-date=" + date;
    }

    public static String getActivitiesUrl(String type) {
        return patientUrl + "/" + Hector.currentPatientId + "/activities?level=" + type;
    }

    public static String getNotificationsUrl(String date) {
        return patientUrl + "/" + Hector.currentPatientId + "/notifications?start-date=" + date + "&end-date=" + date;
    }

    public static String getAllAssociatedPatients(String userId) {
        return userUrl + "/" + userId + "/patients";
    }

    public static String getConfigureDeviceWithFsblUrl(String smartConnector_id, String fsbl_id) {
        return deviceUrl + "/smart-connector/" + smartConnector_id + "/fsbl/" + fsbl_id;
    }

    public static String getConfigureDeviceWithWifiUrl(String smartConnector_id) {
        return deviceUrl + "/smart-connector/" + smartConnector_id + "/wifi";
    }

    public static String getAssociatedFsbl(String deviceId) {
        return patientUrl + "/blisters?deviceId=" + deviceId;
    }

}
