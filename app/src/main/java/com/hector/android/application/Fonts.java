package com.hector.android.application;

import android.graphics.Typeface;

/**
 * Created by arjun on 11/20/15.
 */
public class Fonts {

    public static Typeface BOLD;
    public static Typeface REGULAR;
    public static Typeface MEDIUM;


    public static void initialiseFonts() {
        BOLD = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Font-Heavy.ttf");
        REGULAR = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Font-Normal.ttf");
        MEDIUM = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Font-Medium.ttf");
    }
}
