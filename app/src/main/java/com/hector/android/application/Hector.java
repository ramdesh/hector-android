package com.hector.android.application;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;

import com.hector.android.activities.LauncherActivity;

/**
 * Created by arjun on 11/20/15.
 */
public class Hector extends Application {
    public static final boolean DEBUG = false;
    public static Context applicationContext;
    public static String apiToken;
    public static String deviceToken;
    public static String rootUserId;
    public static String rootUserName;
    public static String currentPatientId;
    public static String currentPatientName;
    public static int toolBarLayoutHeight;
    public static boolean preferToShowNotification;
    public static boolean isConnectedToInternet = true;
    private Thread.UncaughtExceptionHandler defaultUEH;

    public static final String TAG = Hector.class.getSimpleName();


    @Override
    public void onCreate() {
        super.onCreate();
        this.applicationContext = getApplicationContext();
        Fonts.initialiseFonts();
        loadPreferenceValues();
        deterMineToolBarSize();
    }

    private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler =
            new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex) {
                    // here I do logging of exception to a db
                    PendingIntent myActivity = PendingIntent.getActivity(applicationContext,
                            214365, new Intent(applicationContext, LauncherActivity.class),
                            PendingIntent.FLAG_ONE_SHOT);

                    AlarmManager alarmManager;
                    alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                            15000, myActivity );
                    System.exit(2);

                    // re-throw critical exception further to the os (important)
                    defaultUEH.uncaughtException(thread, ex);
                }
            };

    private void deterMineToolBarSize() {
        TypedValue typedValue = new TypedValue();
        if(getTheme().resolveAttribute(android.R.attr.actionBarSize, typedValue, true)){
            toolBarLayoutHeight = TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics());
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
                toolBarLayoutHeight += 13;
            }
        }
    }

    private void loadPreferenceValues() {
        SharedPreferences applicationPreferences = SharedPreferencesManager.getApplicationPreference();
        apiToken = applicationPreferences.getString(SharedPreferencesManager.API_TOKEN, "");
        deviceToken = applicationPreferences.getString(SharedPreferencesManager.DEVICE_TOKEN, "");
        rootUserId = applicationPreferences.getString(SharedPreferencesManager.ROOT_USER_ID, "");
        rootUserName = applicationPreferences.getString(SharedPreferencesManager.ROOT_USER_NAME, "");
        currentPatientName = applicationPreferences.getString(SharedPreferencesManager.CURRENT_USER_NAME, "");
        currentPatientId = applicationPreferences.getString(SharedPreferencesManager.CURRENT_USER_ID, "");
        preferToShowNotification = applicationPreferences.getBoolean(SharedPreferencesManager.NOTIFICATION_PREFERENCE, true);
    }

    public static boolean isUserLoggedIn(){
        if(DEBUG){
            return true;
        }
        return !apiToken.isEmpty();
    }

}
