package com.hector.android.application;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.hector.android.R;
import com.hector.android.activities.LauncherActivity;
import com.hector.android.libs.TypeFaceTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by arjun on 12/4/15.
 */
public class Utils {
    public static final String X_APP_ID = "hectorPortal";
    public static final String AUTHORIZATION = "Basic aGVjdG9yUG9ydGFsOns0YWI4NDQwZC01YjVmLTQyZTktYTY4Ni0xNjM4ODYwZDA5ZDR9";

    private static String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    public static Map<String, String> activityActionStringMap = new HashMap<String, String>(){{
        put("medication_taken", " - TAKEN");
        put("medication_missed", " - MISSED");
        put("medication_early", " - EARLY");
        put("medication_late", " - LATE");
        put("medication_reminder", " - DUE");
    }
    };
    public static Map<String, Integer> activityTypeIconMap = new HashMap<String, Integer>(){{
      put("info", R.drawable.icon_medicaition_info);
      put("warning", R.drawable.icon_medication_warning);
      put("critical", R.drawable.icon_medicaition_alert);
    }};
    public static Map<String, Integer> activityTypeColorMap = new HashMap<String, Integer>(){{
        put("info", R.color.info_activity_color);
        put("warning", R.color.warning_activity_color);
        put("critical", R.color.critical_activity_color);
    }};
    private static AlertDialog alertDialog;

    public static void showToast(String message, int duration){
        Toast.makeText(Hector.applicationContext, message, duration).show();
    }

    public static void showSnackBar(View view, String message, int duration){
        if(view == null || message == null){return;}
        try {
            Snackbar.make(view, message, duration).show();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public static String getTimeFormatA(String dateString) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        try {
            Date date = inputFormat.parse(dateString);
            SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mm a");
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String getTimeFormatB(String dateString) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = inputFormat.parse(dateString);
            SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mm a");
            outputFormat.setTimeZone(TimeZone.getDefault());
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getTimeFormatC(String dateString) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("HH:mm:ss");
        try {
            Date date = inputFormat.parse(dateString);
            SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mm a");
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getTimeFormatD(String dateString) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = inputFormat.parse(dateString);
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM");
            outputFormat.setTimeZone(TimeZone.getDefault());
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getRelativeTime(String createdAt) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        long milliseconds = System.currentTimeMillis();
        try {
            Date date = inputFormat.parse(createdAt);
            milliseconds = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return String.valueOf(DateUtils.getRelativeTimeSpanString(milliseconds));
    }


    public static String getTimeStamp(String createdAt) {//2015-10-14T13:14:46+00:00
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            Date date = inputFormat.parse(createdAt);
            SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd yyyy");
            return outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void restartApplication(Context context){
        Intent mStartActivity = new Intent(context, LauncherActivity.class);
        mStartActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    public static void showLoadingDialog(Context context, String loadingText, boolean isCancelable){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_progress, null);
        builder.setView(view);
        TypeFaceTextView loadingTextView = (TypeFaceTextView) view.findViewById(R.id.loading_dialog_text);
        loadingTextView.setText(loadingText);
        alertDialog = builder.create();
        alertDialog.setCancelable(isCancelable);
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.show();
    }
    public static void dismissLoadingDialog(){
        alertDialog.dismiss();
    }

    public static void logOutUser(Context context) {
        SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.API_TOKEN, "");
        SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.DEVICE_TOKEN, "");
        SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.PROPERTY_GCM_REG_ID, "");
        SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.FEELING_INDEX_NEXT_TIME, "");
        SharedPreferencesManager.clearData();
        restartApplication(context);
    }

    public static boolean isEmailValid(String userEnteredEmail) {
        return userEnteredEmail.matches(EMAIL_REGEX);
    }

    public static void showAlertDialog(Context context, String title, String message, boolean singleButton,
                                       String positiveButtonText, String negativeButtonText,
                                       final DialogClickListener dialogClickListener){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View dialogRootView = LayoutInflater.from(context).inflate(R.layout.alert_dialog, null);
        TypeFaceTextView messageView = (TypeFaceTextView) dialogRootView.findViewById(R.id.dialog_message_text);
        TypeFaceTextView titleView = (TypeFaceTextView) dialogRootView.findViewById(R.id.dialog_title_text);
        TypeFaceTextView negativeBtn = (TypeFaceTextView) dialogRootView.findViewById(R.id.dialog_negative_btn);
        TypeFaceTextView positiveBtn = (TypeFaceTextView) dialogRootView.findViewById(R.id.dialog_positive_btn);
        titleView.setText(title);
        messageView.setText(message);
        positiveBtn.setText(positiveButtonText);
        negativeBtn.setText(negativeButtonText);
        if(singleButton) {
            negativeBtn.setVisibility(View.GONE);
        }
        builder.setView(dialogRootView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.show();
        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogClickListener.onPositiveButtonClicked(alertDialog);
            }
        });
        negativeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogClickListener.onNegativeButtonClicked(alertDialog);
            }
        });
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                dialogClickListener.onDialogDismissed(alertDialog);
            }
        });
    }
}
