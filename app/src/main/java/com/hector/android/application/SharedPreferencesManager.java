package com.hector.android.application;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by arjun on 10/13/15.
 */
public class SharedPreferencesManager {
    private static final String PREFERENCE_NAME = "HectorPreferences";
    public static final String API_TOKEN = "com.hector.android.API_TOKEN";
    public static final String DEVICE_TOKEN = "com.hector.android.DEVICE_TOKEN";
    public static final String DEVICE_ID = "com.hector.android.DEVICE_ID";
    public static final String ROOT_USER_ID = "com.hector.android.ROOT_USER_ID";
    public static final String ROOT_USER_NAME = "com.hector.android.ROOT_USER_NAME";
    public static final String NOTIFICATION_PREFERENCE = "com.hector.android.NOTIFICATION_PREFERENCE";
    public static final String SMS_NOTIFICATION_PREFERENCE = "com.hector.android.SMS_NOTIFICATION_PREFERENCE";
    public static final String EMAIL_NOTIFICATION_PREFERENCE = "com.hector.android.EMAIL_NOTIFICATION_PREFERENCE";
    public static final String PUSH_NOTIFICATION_PREFERENCE = "com.hector.android.PUSH_NOTIFICATION_PREFERENCE";
    public static final String CURRENT_USER_ID = "com.hector.android.CURRENT_USER_ID";
    public static final String CURRENT_USER_NAME = "com.hector.android.CURRENT_USER_NAME";
    public static final String PATIENT_BLISTER_ID = "com.hector.android.PATIENT_BLISTER_ID";
    public static final String PROPERTY_GCM_REG_ID = "com.hector.android.PROPERTY_GCM_REG_ID";
    public static final String USERS_WITH_UNSEEN_ACTIVITIES = "com.hector.android.USERS_WITH_UNSEEN_ACTIVITIES";
    public static final String USER_ID = "com.hector.android.USER_ID";
    public static final String PATIENT_ID = "com.hector.android.PATIENT_ID";
    public static final String hamburger_Showcase = "com.hector.android.hamburgerShowcase";
    public static final String deviceConfig_showcase = "com.hector.android.deviceConfigShowcase";
    public static final String intro_showcase = "com.hector.android.introShowcase";
    public static final String wifiConfig_showcase = "com.hector.android.wifiConfigShowcase";
    public static final String activities_showcase = "com.hector.android.activitiesShowcase";
    public static final String schedule_showcase = "com.hector.android.scheduleShowcase";
    public static final String USER_TYPE = "com.hector.android.userType";
    public static final String REMINDER = "com.hector.android.Reminder";
    public static final String FEELING_INDEX_NEXT_TIME = "com.hector.android.FeelingIndex";

    /**
     * Set or update Shared preference of type {@code String}
     *
     * @param key   SharedPreference key
     * @param value Values to be stored
     */
    public static void setStringPreferenceData(String key, String value) {
        SharedPreferences.Editor editor = Hector.applicationContext
                .getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void setStringPreferenceData(Map<String, String> preferenceData){
        SharedPreferences.Editor editor = Hector.applicationContext
                .getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        for(String key : preferenceData.keySet()){
            editor.putString(key, preferenceData.get(key));
        }
        editor.commit();
    }

    public static void setBooleanPreference(Map<String, Boolean> preferenceData){
        SharedPreferences.Editor editor = Hector.applicationContext
                .getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        for(String key : preferenceData.keySet()){
            editor.putBoolean(key, preferenceData.get(key));
        }
        editor.commit();
    }

    /**
     * get Shared preference value of type {@code String}
     *
     * @param key          SharedPreference key
     * @param defaultValue default value to be returned when there in not data
     */
    public static String getStringPreference(String key, String defaultValue) {
        SharedPreferences preferences = Hector.applicationContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(key, defaultValue);
    }

    /**
     * Set or update Shared preference of type {@code Integer}
     *
     * @param key   SharedPreference key
     * @param value Values to be stored
     */
    public static void setIntPreferenceData(String key, int value) {
        SharedPreferences.Editor editor = Hector.applicationContext
                .getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * Set or update Shared preference of type {@code boolean}
     *
     * @param key   SharedPreference key
     * @param value Values to be stored
     */
    public static void setBooleanPreference(String key, boolean value) {
        SharedPreferences.Editor editor = Hector.applicationContext
                .getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * get Shared preference value of type {@code boolean }
     *
     * @param key          SharedPreference key
     * @param defaultValue default value to be returned when there in not data
     */
    public static boolean getBooleanPreference(String key, boolean defaultValue) {
        SharedPreferences preferences = Hector.applicationContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(key, defaultValue);
    }

    public static Set<String> getStringSetPreference(String key) {
        SharedPreferences preferences = Hector.applicationContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return new HashSet<>(preferences.getStringSet(key, new HashSet<String>()));
    }

    /**
     * get Shared preference value of type {@code Integer}
     *
     * @param key          SharedPreference key
     * @param defaultValue default value to be returned when there in not data
     */
    public static int getIntPreference(String key, int defaultValue) {
        SharedPreferences preferences = Hector.applicationContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return preferences.getInt(key, defaultValue);
    }

    /**
     * Set or update Shared preference of type {@code Long}
     *
     * @param key   SharedPreference key
     * @param value Values to be stored
     */
    public static void setLongPreferenceData(String key, long value) {
        SharedPreferences.Editor editor = Hector.applicationContext
                .getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.putLong(key, value);
        editor.commit();
    }

    /**
     * get Shared preference value of type {@code Long}
     *
     * @param key          SharedPreference key
     * @param defaultValue default value to be returned when there in not data
     */
    public static long getLongPreference(String key, long defaultValue) {
        SharedPreferences preferences = Hector.applicationContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return preferences.getLong(key, defaultValue);
    }

    public static void setStringSetPreference(String key, Set<String> value) {
        SharedPreferences.Editor editor = Hector.applicationContext
                .getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.putStringSet(key, value);
        editor.commit();
    }

    public static void clearData() {
        SharedPreferences.Editor editor = Hector.applicationContext
                .getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.clear();
    }

    public static SharedPreferences getApplicationPreference(){
      return Hector.applicationContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }
}
