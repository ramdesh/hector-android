package com.hector.android.libs;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by arjun on 11/26/15.
 */
public abstract class MyPagerAdapter  extends FragmentPagerAdapter{
    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public abstract int getTabTextColor();
}
