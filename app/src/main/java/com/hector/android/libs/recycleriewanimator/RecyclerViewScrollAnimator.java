package com.hector.android.libs.recycleriewanimator;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;

public class RecyclerViewScrollAnimator implements RecyclerViewItemOnScrollAnimator{
    private final LinearLayoutManager mLayoutManager;
    private final RecyclerView mRecyclerView;
    private final Interpolator mInterpolator;
    int mLastAnimatedPosition = RecyclerView.NO_POSITION;

    public RecyclerViewScrollAnimator(RecyclerView recyclerView) {
        mLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        mRecyclerView = recyclerView;
        mInterpolator = new AccelerateInterpolator(2);
    }

    @Override
    public void onAnimateViewHolder(View view, int position) {
        if(position == 1){
            mLastAnimatedPosition = 1;
        }
//        view.setPivotX(0);
        onPrepareToAnimateViewHolder(view, position);
        if(shouldAnimateOrPrepare(position)){
            ViewCompat.animate(view)
                    .alpha(1)
                    .setInterpolator(mInterpolator)
                    .setDuration(200).start();
            mLastAnimatedPosition = position;
        }
    }

    private boolean shouldAnimateOrPrepare(int position) {
        Log.i("Postion debug ", "Last " + mLastAnimatedPosition + " now " + position);
        return mLastAnimatedPosition == RecyclerView.NO_POSITION || mLastAnimatedPosition < position;
    }

    @Override
    public void onPrepareToAnimateViewHolder(View view, int position) {
        // do some stuff if needed before animation
        if(shouldAnimateOrPrepare(position)){
            ViewCompat.setAlpha(view, 0);
        }
    }
    //clear animation if needed
    private void cancelAnimationAt(int position) {
        RecyclerView.ViewHolder v =  mRecyclerView.findViewHolderForPosition(position);
        if(!isVisible(position) || v != null){
            if(v != null) {
                Log.i("cancelAnimationAt", " canceling " + position);
                //cancelAnimation(v.itemView);
            }
        }

    }

    private void cancelAnimation(View v) {
        if(v != null) {
            v.clearAnimation();
            v.setAnimation(null);
            ViewCompat.animate(v).cancel();
        }
    }

    private boolean isVisible(int position){
        return position >= mLayoutManager.findFirstVisibleItemPosition() && position <= mLayoutManager.findLastVisibleItemPosition();
    }

}