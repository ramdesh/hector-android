package com.hector.android.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.hector.android.R;
import com.hector.android.activities.ActivitiesActivity;
import com.hector.android.adapters.ActivitiesAdapter;
import com.hector.android.application.Hector;
import com.hector.android.application.NetworkStateReceivers;
import com.hector.android.application.Utils;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.requests.ActivitiesRequest;
import com.hector.android.pojo.responses.patientactivities.ActivitiesResponse;
import com.hector.android.pojo.responses.patientactivities.Activity;
import com.hector.android.pojo.responses.patientactivities.dayActivities;

import java.util.ArrayList;

/**
 * Created by arjun on 11/23/15.
 */
public class ActivitiesFragment extends Fragment {
    private final int PAGE_OFFSET = 30;
    private View mRootView;
    private RecyclerView mActivitiesLv;
    private ActivitiesAdapter mActivitiesAdapter;
    private ActivitiesActivity.ScrollListener scrollListener;
    private BroadcastReceiver newMessageReceiver;
    private Gson gson = new Gson();
    private MessageListener messageListener;
    private ArrayList<Activity> activities = new ArrayList<>();
    private String type;
    private String metaEndTime = "";
    private int pageNo = 1;
    private ProgressBar mLoadingPb;
    private TypeFaceTextView mNoDateLabel;
    private boolean mMoreDataAvailable = true;
    private LinearLayoutManager linearLayoutManager;
    private boolean mRequestInProgress = false;
    private Activity loadingActivity = new Activity(ActivitiesAdapter.VIEW_TYPE_LOADING);
    private String previous = "";
    private BroadcastReceiver networkStateReceiver;


    public static ActivitiesFragment getFragmentInstance(ActivitiesActivity.ScrollListener scrollListener, MessageListener messageListener, String activityType) {
        ActivitiesFragment activitiesFragment = new ActivitiesFragment();
        activitiesFragment.scrollListener = scrollListener;
        activitiesFragment.messageListener = messageListener;
        activitiesFragment.type = activityType;
        return activitiesFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        registerNetWorkStateReceiver();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_activities, container, false);
        initialiseViews();
        bindEvents();
        if (Hector.isConnectedToInternet) {
            fetchActivities();
        }

        return mRootView;
    }

    private void registerNetWorkStateReceiver() {
        networkStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Hector.isConnectedToInternet && activities.isEmpty()) {
                    fetchActivities();
                }
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(networkStateReceiver, new IntentFilter(NetworkStateReceivers.NETWORK_CONNECTION_STATE_CHANGED));
    }

    private void fetchActivities() {
        new ActivitiesRequest().getActivities(getActivity(), type, metaEndTime, new MyHttpCallback<ActivitiesResponse>() {
            @Override
            public void success(ActivitiesResponse data) {
                if (pageNo == 1) {
                    mLoadingPb.setVisibility(View.GONE);
                    if (data.dayActivities.size() == 0) {
                        mNoDateLabel.setVisibility(View.VISIBLE);
                        return;
                    }
                    activities.clear();
                } else {
                    activities.remove(loadingActivity);
                }

                ArrayList<Activity> activities1 = extractActivityFromResponse(data.dayActivities);
                activities.addAll(processResponse(activities1));
                if (activities1.size() < PAGE_OFFSET) {
                    mMoreDataAvailable = false;
                    activities.add(new Activity(ActivitiesAdapter.VIEW_TYPE_END_OF_LIST));
                } else {
                    pageNo++;
                }

                mActivitiesAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(String msg) {
                mLoadingPb.setVisibility(View.GONE);
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
            }

            @Override
            public void onBefore() {
                mRequestInProgress = true;
                if (pageNo == 1) {
                    mLoadingPb.setVisibility(View.VISIBLE);
                } else {
                    activities.add(loadingActivity);
                    mActivitiesAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFinish() {
                mRequestInProgress = false;
            }
        });
    }

    private ArrayList<Activity> extractActivityFromResponse(ArrayList<dayActivities> dayActivitiesArrayList) {
        ArrayList<Activity> activityArrayList = new ArrayList<>();

        for (int i = 0; i < dayActivitiesArrayList.size(); i++) {
            dayActivities dayactivities = new dayActivities();
            dayactivities = dayActivitiesArrayList.get(i);
            activityArrayList.addAll(dayactivities.activities);
        }
        return activityArrayList;
    }

    private ArrayList<Activity> processResponse(ArrayList<Activity> activities) {
        ArrayList<Activity> activityArrayList = new ArrayList<>();
        for (Activity activity : activities) {
            if (activity.dateTime != null) {
                String timeStamp = Utils.getTimeStamp(activity.dateTime);  // was activity.createdAt
                if (!previous.equals(timeStamp)) {
                    previous = timeStamp;
                    activityArrayList.add(new Activity(ActivitiesAdapter.VIEW_TYPE_SECTION, timeStamp));
                }
                activityArrayList.add(activity);
            }
        }
        return activityArrayList;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(Hector.applicationContext).unregisterReceiver(newMessageReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(networkStateReceiver);
    }

    private void bindEvents() {
        mActivitiesLv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int totalUpDistance = 0;
            int totalDownDistance = 0;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.i("Scrolled ", dy + "");
                if (dy > 0) {
                    totalDownDistance = 0;
                    totalUpDistance += dy;
                    if (totalUpDistance > ActivitiesActivity.SCROLL_UP_THRESHOLD) {
                        scrollListener.onScrolledUp();
                        checkForEndOfList();
                    }
                } else if (dy < 0) {
                    totalUpDistance = 0;
                    totalDownDistance += dy;
                    if (totalDownDistance < ActivitiesActivity.SCROLL_DOWN_THRESHOLD) {
                        scrollListener.onScrolledDown();
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void checkForEndOfList() {
        int visibleItemCount = linearLayoutManager.getChildCount();
        int totalItemCount = linearLayoutManager.getItemCount();
        int firstVisibleItemPos = linearLayoutManager.findFirstVisibleItemPosition();
        if (!mRequestInProgress
                && mMoreDataAvailable
                && (visibleItemCount + firstVisibleItemPos) >= totalItemCount
                && Hector.isConnectedToInternet) {
            Log.i("End of list reached ", "fetching data");
            fetchActivities();
        }
    }

    private void initialiseViews() {
        mLoadingPb = (ProgressBar) mRootView.findViewById(R.id.activities_load_pb);
        mNoDateLabel = (TypeFaceTextView) mRootView.findViewById(R.id.empty_data_ph);
        mActivitiesLv = (RecyclerView) mRootView.findViewById(R.id.activities_list_view);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        mActivitiesLv.setLayoutManager(linearLayoutManager);
        mActivitiesAdapter = new ActivitiesAdapter(getActivity(), activities, mActivitiesLv);
        mActivitiesLv.setAdapter(mActivitiesAdapter);
    }

    public ActivitiesFragment setMessageListener(MessageListener messageListener) {
        this.messageListener = messageListener;
        return this;
    }

    public interface MessageListener {
        void messageReceived(ActivitiesActivity.fragmentsEnum position);
    }
}
