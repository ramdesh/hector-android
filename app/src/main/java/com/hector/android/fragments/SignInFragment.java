package com.hector.android.fragments;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.hector.android.R;
import com.hector.android.activities.LauncherActivity;
import com.hector.android.activities.SwitchPatientsActivity;
import com.hector.android.application.Utils;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.buttonwithprogressbar.CircularProgressButton;
import com.hector.android.pojo.requests.CreateSessionRequest;
import com.hector.android.pojo.requests.DeviceRegRequest;
import com.hector.android.pojo.responses.CreateSessionResponse;
import com.hector.android.pojo.responses.UserDetailResponse;
import com.hector.android.pojo.responses.pateintdevicedetails.DeviceRegResponse;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by arjun on 11/24/15.
 */
public class SignInFragment extends Fragment {
    private View mRootView;
    private AutoCompleteTextView mUserEmailEt;
    private EditText mPasswordEt;
    private CircularProgressButton mSubmitBtn;
    private boolean requestInProgress = false;
    private Set<String> availableEmails = new HashSet<>();
    private ArrayAdapter<String> emailAdapter;
    private String[] user_type = {"patient", "caregiver"};

    public SignInFragment() {
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_sign_in, container, false);
        initialiseView();
        bindEvents();
        return mRootView;
    }

    private void bindEvents() {
        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = mUserEmailEt.getText().toString();
                String password = mPasswordEt.getText().toString();
                if (isValidData(username, password)) {
                    mSubmitBtn.setProgress(50);
                    createSession(username, password);
                }
            }
        });
    }

    private void createSession(String username, String password) {
        new CreateSessionRequest().createSession(getActivity(), username, password, new MyHttpCallback<CreateSessionResponse>() {
            @Override
            public void success(CreateSessionResponse data) {

                if (data.userType.equals(user_type[0]))
                    fetchPatientDetails();
                if (data.userType.equals(user_type[1]))
                    fetchCaregiverDetails();
            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                mSubmitBtn.setProgress(0);
                requestInProgress = false;
            }

            @Override
            public void onBefore() {
                requestInProgress = true;
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void registerDevice() {
        new DeviceRegRequest().getDevice(getActivity(), new MyHttpCallback<DeviceRegResponse>() {
            @Override
            public void success(DeviceRegResponse data) {
            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                mSubmitBtn.setProgress(0);
                requestInProgress = false;
            }

            @Override
            public void onBefore() {
                Utils.showSnackBar(mRootView, getResources().getString(R.string.fetching_device_id), Snackbar.LENGTH_LONG);
            }

            @Override
            public void onFinish() {
                /*
                    do not use this
                */
            }
        });
    }

    private void fetchCaregiverDetails(){

        new CreateSessionRequest().getCaregiverDetails(getActivity(), new MyHttpCallback<UserDetailResponse>() {
            @Override
            public void success(UserDetailResponse data) {
                Intent switchPatientIntent = new Intent(getActivity(), LauncherActivity.class);
                switchPatientIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(switchPatientIntent);
                getActivity().finish();
            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                mSubmitBtn.setProgress(0);
                requestInProgress = false;
            }

            @Override
            public void onBefore() {
                Utils.showSnackBar(mRootView, getResources().getString(R.string.fetching_user_details), Snackbar.LENGTH_LONG);
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void fetchPatientDetails() {
        new CreateSessionRequest().getUserDetails(getActivity(), new MyHttpCallback<UserDetailResponse>() {
            @Override
            public void success(UserDetailResponse data) {
                Intent dashboardIntent = new Intent(getActivity(), LauncherActivity.class);
                dashboardIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(dashboardIntent);
                getActivity().finish();
            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                mSubmitBtn.setProgress(0);
                requestInProgress = false;
            }

            @Override
            public void onBefore() {
                Utils.showSnackBar(mRootView, getResources().getString(R.string.fetching_user_details), Snackbar.LENGTH_LONG);
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private boolean isValidData(String userEmail, String password) {
        if (userEmail.isEmpty() || !Utils.isEmailValid(userEmail)) {
            mUserEmailEt.setError(getResources().getString(R.string.email_validation_error));
            return false;
        }
        if (password.length() < 1) {
            mPasswordEt.setError(getResources().getString(R.string.invalid_password));
            return false;
        }
        return true;
    }

    private void initialiseView() {
        mUserEmailEt = (AutoCompleteTextView) mRootView.findViewById(R.id.sign_in_username);
        mPasswordEt = (EditText) mRootView.findViewById(R.id.sign_in_password);
        mSubmitBtn = (CircularProgressButton) mRootView.findViewById(R.id.sign_in_button);
        mSubmitBtn.setIndeterminateProgressMode(true);
        AccountManager accountManager = (AccountManager) getActivity().getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accounts = accountManager.getAccounts();
        for (Account account : accounts) {
            if (Utils.isEmailValid(account.name)) {
                availableEmails.add(account.name);
            }
        }
        emailAdapter = new ArrayAdapter<>(getActivity(), R.layout.single_list_item, new ArrayList(availableEmails));
        mUserEmailEt.setAdapter(emailAdapter);
    }
}
