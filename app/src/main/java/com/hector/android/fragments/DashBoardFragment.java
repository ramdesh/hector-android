package com.hector.android.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.hector.android.R;
import com.hector.android.activities.LauncherDashboardHelper;
import com.hector.android.adapters.ScheduleAdapter;
import com.hector.android.application.Hector;
import com.hector.android.application.NetworkStateReceivers;
import com.hector.android.application.Utils;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.DividerItemDecoration;
import com.hector.android.pojo.requests.SchedulesRequest;
import com.hector.android.pojo.responses.patientschedules.ScheduleResponse;
import com.hector.android.pojo.responses.patientschedules.adherence;
import com.hector.android.pojo.responses.patientschedules.dailyAdherence;

import java.util.ArrayList;

/**
 * Created by arjun on 11/13/15.
 */
public class DashBoardFragment extends Fragment {
    private final String[] timesOfDay = {"morning", "noon", "evening", "bedtime"};
    private View mRootView;
    private RecyclerView scheduleLv;
    private ScheduleAdapter mScheduleAdapter;
    private LauncherDashboardHelper.ScrollListener scrollListener;
    private ArrayList<dailyAdherence> scheduleList = new ArrayList<>();
    private boolean isRequestInProgress;
    private ProgressBar progressBar;
    private BroadcastReceiver networkStateReceiver;
    private String dateToFetch;
    private BroadcastReceiver dateChangeReceiver;

    public static DashBoardFragment getFragmentInstance(LauncherDashboardHelper.ScrollListener scrollListener) {
        DashBoardFragment dashBoardFragment = new DashBoardFragment();
        dashBoardFragment.scrollListener = scrollListener;
        return dashBoardFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        registerNetWorkStateReceiver();
        if (getArguments().getBoolean(LauncherDashboardHelper.DO_REGISTER_RECEIVER)) {
            registerDateChangeReceiver();
        }
    }

    private void registerDateChangeReceiver() {
        dateChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                dateToFetch = intent.getStringExtra(LauncherDashboardHelper.DATE_TO_FETCH);
                fetchScheduleFor();
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(dateChangeReceiver, new IntentFilter(LauncherDashboardHelper.DATE_CHANGED));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(networkStateReceiver);
        if (dateChangeReceiver != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(dateChangeReceiver);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        dateToFetch = getArguments().getString(LauncherDashboardHelper.DATE_TO_FETCH);
        getArguments().clear();
        initialiseViews();
        bindEvents();
        if (dateToFetch != null && !dateToFetch.isEmpty()) {
            Log.i("Date to fetch ", dateToFetch);
            fetchScheduleFor();
        }
        if (isRequestInProgress) {
            progressBar.setVisibility(View.VISIBLE);
        }
        return mRootView;
    }

    private void registerNetWorkStateReceiver() {
        networkStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Hector.isConnectedToInternet && scheduleList.isEmpty()) {
                    fetchScheduleFor();
                }
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(networkStateReceiver, new IntentFilter(NetworkStateReceivers.NETWORK_CONNECTION_STATE_CHANGED));
    }

    private void fetchScheduleFor() {
        if (isRequestInProgress) {
            return;
        }
        new SchedulesRequest().getSchedulesOn(getActivity(), dateToFetch, new MyHttpCallback<ScheduleResponse>() {
            @Override
            public void success(ScheduleResponse data) {
                processResponse(data.adherences);
                mScheduleAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
            }

            @Override
            public void onBefore() {
                isRequestInProgress = true;
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progressBar.setVisibility(View.GONE);
                isRequestInProgress = false;
            }
        });
    }

    private void processResponse(ArrayList<adherence> schedules) {
        scheduleList.clear();
        if (schedules == null || schedules.isEmpty()) {
            for (int index = 0; index < 4; index++) {
                dailyAdherence empty_dailyAdherence = new dailyAdherence();
                empty_dailyAdherence.time = timesOfDay[index];
                scheduleList.add(empty_dailyAdherence);
            }
        } else {
            ArrayList<dailyAdherence> dailyAdherences = schedules.get(0).dailyAdherences;
            int index;
            outerLoop:
            for (index = 0; index < 4; index++) {
                for (dailyAdherence dailyadherence : dailyAdherences) {
                    if (dailyadherence.time.equals(timesOfDay[index])) {
                        scheduleList.add(dailyadherence);
                        dailyAdherences.remove(dailyadherence);
                        continue outerLoop;
                    }
                }
                dailyAdherence empty_dailyAdherence = new dailyAdherence();
                empty_dailyAdherence.time = timesOfDay[index];
                scheduleList.add(empty_dailyAdherence);
            }
        }

    }

    private void bindEvents() {

    }

    private void initialiseViews() {
        progressBar = (ProgressBar) mRootView.findViewById(R.id.schedule_fetch_progress);
        scheduleLv = (RecyclerView) mRootView.findViewById(R.id.schedule_lv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        scheduleLv.setLayoutManager(linearLayoutManager);
        scheduleLv.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.recyclerview_divider)));
        mScheduleAdapter = new ScheduleAdapter(getActivity(), scheduleList);
        scheduleLv.setAdapter(mScheduleAdapter);
    }

}
