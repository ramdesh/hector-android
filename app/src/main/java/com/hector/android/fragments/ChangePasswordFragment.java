package com.hector.android.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.hector.android.R;
import com.hector.android.application.DialogClickListener;
import com.hector.android.application.Utils;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.requests.ChangePwRequest;
import com.hector.android.pojo.responses.ChangePwResponse;

/**
 * Created by arjun on 12/28/15.
 */
public class ChangePasswordFragment extends Fragment {
    private View mRootView;
    private EditText currentPasswordText;
    private EditText newPasswordText;
    private EditText newPasswordTextAgain;
    private TypeFaceTextView changePwBtn;
    private String enteredPw;
    private String enteredNewPw;
    private String reEnteredNewPw;
    private InputMethodManager imm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_change_pw, container, false);
        initialiseViews();
        bindEvents();
        return mRootView;
    }

    private void initialiseViews() {
        currentPasswordText = (EditText) mRootView.findViewById(R.id.change_pw_oldpw_et);
        newPasswordText = (EditText) mRootView.findViewById(R.id.change_pw_newpw_et);
        newPasswordTextAgain = (EditText) mRootView.findViewById(R.id.change_pw_re_newpw_et);
        changePwBtn = (TypeFaceTextView) mRootView.findViewById(R.id.change_pw_dialog_positive_btn);
        currentPasswordText.requestFocus();
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private void bindEvents() {
        changePwBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDataValid()) {
                    imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(currentPasswordText.getWindowToken(), 0);
                    ChangePwRequest changePwRequest = new ChangePwRequest(enteredPw, enteredNewPw);
                    changePwRequest.changePassword(getActivity(), new MyHttpCallback<ChangePwResponse>() {
                        @Override
                        public void success(ChangePwResponse data) {
                            if (data.body.isRequestSuccess) {
                                showSuccessDialog();
                            } else {
                                Utils.showSnackBar(mRootView, "Password change failed", Snackbar.LENGTH_LONG);
                            }
                        }

                        @Override
                        public void failure(String msg) {
                            msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                            Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                        }

                        @Override
                        public void onBefore() {
                            Utils.showLoadingDialog(getActivity(), getResources().getString(R.string.please_wait), false);
                        }

                        @Override
                        public void onFinish() {
                            Utils.dismissLoadingDialog();
                        }
                    });
                }
            }
        });
    }

    private void showSuccessDialog() {
        Utils.showAlertDialog(getActivity(),
                getResources().getString(R.string.successful),
                getResources().getString(R.string.pw_changed),
                true, getResources().getString(R.string.ok),
                getResources().getString(R.string.empty), new DialogClickListener() {
                    @Override
                    public void onPositiveButtonClicked(AlertDialog alertDialog) {
                        alertDialog.dismiss();
                        getActivity().finish();
                    }

                    @Override
                    public void onNegativeButtonClicked(AlertDialog alertDialog) {

                    }

                    @Override
                    public void onDialogDismissed(AlertDialog alertDialog) {

                    }
                });
    }

    public boolean isDataValid() {
        boolean isValid = true;
        enteredPw = currentPasswordText.getText().toString().trim();
        enteredNewPw = newPasswordText.getText().toString().trim();
        reEnteredNewPw = newPasswordTextAgain.getText().toString().trim();
        if (enteredPw.length() < 8) {
            currentPasswordText.setError(getResources().getString(R.string.invalid_password));
            isValid = false;
        }
        if (enteredNewPw.length() < 8) {
            newPasswordText.setError(getResources().getString(R.string.invalid_password));
            isValid = false;
        }
        if (reEnteredNewPw.length() < 8) {
            newPasswordTextAgain.setError(getResources().getString(R.string.invalid_password));
            isValid = false;
        }
        if (!enteredNewPw.equals(reEnteredNewPw)) {
            newPasswordTextAgain.setError(getResources().getString(R.string.password_missmatch));
            isValid = false;
        }
        return isValid;
    }
}
