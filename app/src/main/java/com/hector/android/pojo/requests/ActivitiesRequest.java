package com.hector.android.pojo.requests;

import android.content.Context;
import android.util.Log;

import com.hector.android.application.Hector;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.patientactivities.ActivitiesResponse;

/**
 * Created by arjun on 12/3/15.
 */
public class ActivitiesRequest {
    private static final String LOG_TAG = "ActivitiesRequest";

    public void getActivities(final Context context, String type, String timeStamp, final MyHttpCallback<ActivitiesResponse> activitiesResponseCallback){
        MyHttp.getInstance(context, Endpoints.getActivitiesUrl(type), MyHttp.GET, ActivitiesResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .send(new MyHttpCallback<ActivitiesResponse>() {
                    @Override
                    public void success(ActivitiesResponse data) {
                        if(data.code == null){
                            Log.i(LOG_TAG, "Activities request success " + data.toString());
                            activitiesResponseCallback.success(data);
                        } else {
                            Log.i(LOG_TAG, "Activities request error" + data.message);
                            activitiesResponseCallback.failure(data.message);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        Log.i(LOG_TAG, "Activities request failed " + msg);
                        activitiesResponseCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        activitiesResponseCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        activitiesResponseCallback.onFinish();
                    }
                });
    }
}
