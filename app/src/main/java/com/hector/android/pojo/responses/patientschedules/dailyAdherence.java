package com.hector.android.pojo.responses.patientschedules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by amal on 20/01/16.
 */
public class dailyAdherence {
    @Expose
    @SerializedName("slot")
    public int slot;

    @Expose
    @SerializedName("time")
    public String time;

    @Expose
    @SerializedName("actualTime")
    public String actualTime;

    @Expose
    @SerializedName("status")
    public String status;

    @Expose
    @SerializedName("adherenceLevel")
    public String adherenceLevel;

    @Expose
    @SerializedName("medications")
    public ArrayList<Medication> medications;

    public class Medication{
        @Expose
        @SerializedName("id")
        public String id;

        @Expose
        @SerializedName("drug")
        public String drug;

        @Expose
        @SerializedName("quantity")
        public String quantity;

        @Expose
        @SerializedName("description")
        public String description;
    }
}
