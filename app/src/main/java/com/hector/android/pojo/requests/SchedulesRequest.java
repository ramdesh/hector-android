package com.hector.android.pojo.requests;

import android.content.Context;
import android.util.Log;

import com.hector.android.application.Hector;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.patientschedules.ScheduleResponse;

/**
 * Created by arjun on 11/26/15.
 */
public class SchedulesRequest {
    private static final String LOG_TAG = "Schedule response";

    public void getSchedulesOn(Context context, final String dateToFetch, final MyHttpCallback<ScheduleResponse> scheduleResponseCallback){
        MyHttp.getInstance(context,
                Endpoints.getSchedulesOn(dateToFetch),
                MyHttp.GET,
                ScheduleResponse.class)
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .setDefaultHeaders()
                .send(new MyHttpCallback<ScheduleResponse>() {
                    @Override
                    public void success(ScheduleResponse data) {
                        if(data.code == null) {
                            scheduleResponseCallback.success(data);
                        } else {
                            scheduleResponseCallback.failure(data.message);
                        }
                        Log.i(LOG_TAG, data.toString());
                    }

                    @Override
                    public void failure(String msg) {
                        scheduleResponseCallback.failure(msg);
                        Log.i(LOG_TAG, "Failed " + msg);
                    }

                    @Override
                    public void onBefore() {
                        scheduleResponseCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        scheduleResponseCallback.onFinish();
                    }
                });
    }
}
