package com.hector.android.pojo.responses.patientactivities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hector.android.pojo.responses.BaseHttpResponse;
import com.hector.android.pojo.responses.ErrorHttpResponse;

import java.util.ArrayList;

/**
 * Created by arjun on 12/3/15.
 */
public class ActivitiesResponse extends ErrorHttpResponse {

    @Expose
    @SerializedName("dayActivities")
    public ArrayList<dayActivities> dayActivities;

    public ActivityMeta meta;
}
