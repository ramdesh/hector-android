package com.hector.android.pojo.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by arjun on 11/24/15.
 */
public class CreateSessionResponse{

    @Expose
    @SerializedName("token")
    public String token;

    @Expose
    @SerializedName("userId")
    public String userId;

    @Expose
    @SerializedName("userName")
    public String userName;

    @Expose
    @SerializedName("userType")
    public String userType;

    @Expose
    @SerializedName("patientId")
    public String patientId;

    @Expose
    @SerializedName("message")
    public String error_msg;

    @Expose
    @SerializedName("code")
    public String error_code;

}
