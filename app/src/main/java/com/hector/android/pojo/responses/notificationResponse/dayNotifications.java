package com.hector.android.pojo.responses.notificationResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hector.android.pojo.responses.patientactivities.Activity;

import java.util.ArrayList;

/**
 * Created by amal on 22/01/16.
 */
public class dayNotifications {
    @Expose
    @SerializedName("notifications")
    public ArrayList<Activity> activities;

}
