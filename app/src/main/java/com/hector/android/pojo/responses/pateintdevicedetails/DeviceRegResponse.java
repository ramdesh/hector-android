package com.hector.android.pojo.responses.pateintdevicedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hector.android.pojo.responses.BaseHttpResponse;

/**
 * Created by arjun on 11/25/15.
 */
public class DeviceRegResponse extends BaseHttpResponse {
    @Expose
    public Body body;

    public class Body {
        @Expose
        @SerializedName("ok")
        public boolean isResponseOk;

        @Expose
        @SerializedName("_id")
        public String deviceId;

        @Expose
        @SerializedName("rev")
        public String sessionRev;

        @Expose
        public String deviceToken;
    }
}
