package com.hector.android.pojo.responses.patientschedules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by arjun on 11/26/15.
 */
public class Schedule{
    @Expose
    @SerializedName("_id")
    public String scheduleId;

    @Expose
    @SerializedName("_rev")
    public String rev;

    @Expose
    public String blisterId;

    @Expose
    public String status;

    @Expose
    @SerializedName("patientId")
    public String patientId;

    @Expose
    public String intakeDate;

    @Expose
    @SerializedName("timeOfDay")
    public String timeOfDay;

    @Expose
    public String datatype;

    @Expose
    public String createdAt;

    @Expose
    public ArrayList<Medication> medications;

    public class Medication{
        @Expose
        @SerializedName("_id")
        public String id;

        @Expose
        public String drug;

        @Expose
        public String quantity;

        @Expose
        public String description;
    }
}