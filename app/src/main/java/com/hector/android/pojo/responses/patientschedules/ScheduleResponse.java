package com.hector.android.pojo.responses.patientschedules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hector.android.pojo.responses.BaseHttpResponse;
import com.hector.android.pojo.responses.ErrorHttpResponse;

import java.util.ArrayList;

/**
 * Created by arjun on 11/26/15.
 */
public class ScheduleResponse extends ErrorHttpResponse {

    @Expose
    @SerializedName("adherence")
    public ArrayList<adherence> adherences;
}
