package com.hector.android.pojo.responses.karmadetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amal on 18/03/16.
 */
public class personality {

    @Expose
    @SerializedName("negative")
    public double negative;

    @Expose
    @SerializedName("positive")
    public double positive;

    @Expose
    @SerializedName("personalityIndex")
    public double personalityIndex;

}
