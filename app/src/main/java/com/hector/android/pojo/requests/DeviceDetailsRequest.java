package com.hector.android.pojo.requests;

import android.content.Context;

import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.SmartCommFsblConfigDetails;
import com.hector.android.pojo.SmartCommWifiConfigDetails;
import com.hector.android.pojo.responses.pateintdevicedetails.DeviceConfigResponse;
import com.hector.android.pojo.responses.pateintdevicedetails.DeviceDetailsResponse;

/**
 * Created by arjun on 12/12/15.
 */
public class DeviceDetailsRequest {
    public SmartCommWifiConfigDetails smartCommWifiConfigDetails;
    public SmartCommFsblConfigDetails smartCommFsblConfigDetails;

    public void getDevicesOfPatients(Context context, final MyHttpCallback myHttpCallback) {
        MyHttp.getInstance(context, Endpoints.getAssociatedDevice(SharedPreferencesManager.getStringPreference(SharedPreferencesManager.CURRENT_USER_ID, "")), MyHttp.GET, DeviceDetailsResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .send(new MyHttpCallback<DeviceDetailsResponse>() {
                    @Override
                    public void success(DeviceDetailsResponse data) {

                        myHttpCallback.success(data);

                    }

                    @Override
                    public void failure(String msg) {
                        myHttpCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        myHttpCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        myHttpCallback.onFinish();
                    }
                });
    }

    public void configureDeviceWithWifi(Context context, String connectorId, final MyHttpCallback<DeviceConfigResponse> myHttpCallback) {
        MyHttp.getInstance(context, Endpoints.getConfigureDeviceWithWifiUrl(connectorId), MyHttp.POST, DeviceConfigResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .addJson(this.smartCommWifiConfigDetails)
                .send(new MyHttpCallback<DeviceConfigResponse>() {
                    @Override
                    public void success(DeviceConfigResponse data) {
                        if (data.code == null)
                            myHttpCallback.success(data);
                        else {
                            myHttpCallback.failure(data.message);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        myHttpCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        myHttpCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        myHttpCallback.onFinish();
                    }
                });
    }

    public void configureDeviceWithFsbl(Context context, final MyHttpCallback<DeviceConfigResponse> myHttpCallback) {
        MyHttp.getInstance(context, Endpoints.getConfigureDeviceWithFsblUrl(this.smartCommFsblConfigDetails.communicatorId, this.smartCommFsblConfigDetails.fsblId), MyHttp.POST, DeviceConfigResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .addJson(this.smartCommFsblConfigDetails)
                .send(new MyHttpCallback<DeviceConfigResponse>() {
                    @Override
                    public void success(DeviceConfigResponse data) {
                        if (data == null)
                            myHttpCallback.success(data);
                        else myHttpCallback.failure(data.message);
                    }

                    @Override
                    public void failure(String msg) {
                        myHttpCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        myHttpCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        myHttpCallback.onFinish();
                    }
                });
    }
}
