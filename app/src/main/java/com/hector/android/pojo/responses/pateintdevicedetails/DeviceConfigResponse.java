package com.hector.android.pojo.responses.pateintdevicedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hector.android.pojo.responses.BaseHttpResponse;
import com.hector.android.pojo.responses.ErrorHttpResponse;
import com.hector.android.pojo.responses.GenericResponse;

/**
 * Created by arjun on 12/23/15.
 */
public class DeviceConfigResponse extends ErrorHttpResponse {
    @Expose
    public GenericResponse body;

    @Expose
    @SerializedName("deviceId")
    public String deviceId;

    @Expose
    @SerializedName("wifiName")
    public String wifiName;

    @Expose
    @SerializedName("wifiPassword")
    public String wifiPassword;

    @Expose
    @SerializedName("securityType")
    public String securityType;
}
