package com.hector.android.pojo.responses.pateintdevicedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hector.android.pojo.responses.BaseHttpResponse;

/**
 * Created by arjun on 12/15/15.
 */
public class PatientBlisterResponse extends BaseHttpResponse {

    @Expose
    public Blister body;
    public class Blister{
        @Expose
        @SerializedName("_id")
        public String blisterId;

        @Expose
        @SerializedName("connected")
        public boolean isConnected;
    }
}
