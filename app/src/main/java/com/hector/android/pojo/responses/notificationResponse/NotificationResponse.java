package com.hector.android.pojo.responses.notificationResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hector.android.pojo.responses.ErrorHttpResponse;
import com.hector.android.pojo.responses.patientactivities.ActivityMeta;

import java.util.ArrayList;

/**
 * Created by arjun on 12/3/15.
 */
public class NotificationResponse extends ErrorHttpResponse {

    @Expose
    @SerializedName("dayNotifications")
    public ArrayList<dayNotifications> dayActivities;

    public ActivityMeta meta;
}
