package com.hector.android.pojo.responses.patientactivities;

import com.google.gson.annotations.Expose;

/**
 * Created by arjun on 12/14/15.
 */
public class ActivityMeta {
    @Expose
    public String endTime;
}
