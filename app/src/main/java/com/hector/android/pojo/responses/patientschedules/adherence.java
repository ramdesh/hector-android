package com.hector.android.pojo.responses.patientschedules;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by amal on 20/01/16.
 */
public class adherence {
    @Expose
    @SerializedName("date")
    public String date;

    @Expose
    @SerializedName("compliance")
    public boolean compliance;

    @Expose
    @SerializedName("dailyAdherence")
    public ArrayList<dailyAdherence> dailyAdherences;
}
