package com.hector.android.pojo.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amal on 29/03/16.
 */
public class weatherResponse extends ErrorHttpResponse {
    @Expose
    @SerializedName("humidity")
    public String humidity;

    @Expose
    @SerializedName("temperature")
    public String temperature;

    @Expose
    @SerializedName("skyCover")
    public String skyCover;

    @Expose
    @SerializedName("iconCode")
    public int iconCode;
}
