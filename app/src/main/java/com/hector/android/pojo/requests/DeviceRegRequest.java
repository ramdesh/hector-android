package com.hector.android.pojo.requests;

import android.content.Context;

import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.pateintdevicedetails.DeviceRegResponse;

/**
 * Created by arjun on 11/25/15.
 */
public class DeviceRegRequest {
    public String deviceId;

    public void getDevice(final Context context, final MyHttpCallback<DeviceRegResponse> responseCallback){
        MyHttp.getInstance(context, Endpoints.patientDeviceUrl, MyHttp.GET, DeviceRegResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .send(new MyHttpCallback<DeviceRegResponse>() {
                    @Override
                    public void success(DeviceRegResponse data) {
                        if(data.code == MyHttp.REQUEST_OK && data.body.deviceToken != null){
                            saveDataToPhone(data);
                            responseCallback.success(data);
                        } else if(data.code == MyHttp.GET_DEVICE_FAIL) {
                            createDevice(context, responseCallback);
                        } else {
                            responseCallback.failure(data.message);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        responseCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        responseCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {

                    }
                });
    }

    private void saveDataToPhone(DeviceRegResponse data) {
        Hector.deviceToken = data.body.deviceToken;
        SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.DEVICE_TOKEN, data.body.deviceToken);
        SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.DEVICE_ID, data.body.deviceId);
    }

    public void createDevice(Context context, final MyHttpCallback<DeviceRegResponse> responseCallback) {
        MyHttp.getInstance(context, Endpoints.patientDeviceUrl, MyHttp.POST, DeviceRegResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .setHeaders(MyHttp.HEADER_CONTENT_LENGTH, "0")
                .send(new MyHttpCallback<DeviceRegResponse>() {
                    @Override
                    public void success(DeviceRegResponse data) {
                        if (data.code == MyHttp.REQUEST_OK && data.body != null) {
                            saveDataToPhone(data);
                            responseCallback.success(data);
                        } else {
                            responseCallback.failure(data.message);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        responseCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                    }

                    @Override
                    public void onFinish() {
                        responseCallback.onFinish();
                    }
                });
    }
}
