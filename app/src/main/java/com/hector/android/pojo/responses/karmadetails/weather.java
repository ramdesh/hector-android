package com.hector.android.pojo.responses.karmadetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amal on 18/03/16.
 */
public class weather {

    @Expose
    @SerializedName("iconCode")
    public int iconCode;

    @Expose
    @SerializedName("skyCoverIndex")
    public int skyCoverIndex;

    @Expose
    @SerializedName("skyCover")
    public String skyCover;

    @Expose
    @SerializedName("temperature")
    public double temperature;

    @Expose
    @SerializedName("temperatureDescription")
    public String temperatureDescription;

    @Expose
    @SerializedName("temperatureIndex")
    public int temperatureIndex;

    @Expose
    @SerializedName("weatherIndex")
    public double weatherIndex;

    @Expose
    @SerializedName("windSpeed")
    public int windSpeed;

    @Expose
    @SerializedName("windSpeedIndex")
    public int windSpeedIndex;

}
