package com.hector.android.pojo.requests;

import android.content.Context;
import android.util.Log;

import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.CreateSessionResponse;
import com.hector.android.pojo.responses.UserDetailResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by arjun on 11/24/15.
 */
public class CreateSessionRequest {
    public String email;
    public String password;
    public String gender;
    public String firstName;
    public String lastName;


    public void createSession(Context context, String email, String password, final MyHttpCallback myHttpCallback) {
        this.email = email;
        this.password = password;
        MyHttp.getInstance(context, Endpoints.authUrl, MyHttp.POST, CreateSessionResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .addJson(this)
                .send(new MyHttpCallback<CreateSessionResponse>() {
                    @Override
                    public void success(CreateSessionResponse data) {
                        if (data.error_code == null) {
                            Log.i("Response ", data.token);
                            Hector.apiToken = data.token;
                            SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.API_TOKEN, data.token);
                            SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.USER_ID, data.userId);
                            SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.PATIENT_ID, data.patientId);
                            SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.USER_TYPE, data.userType);

                            myHttpCallback.success(data);
                        } else {
                            myHttpCallback.failure(data.error_msg);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        Log.i("Response fail", msg);
                        myHttpCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        myHttpCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        myHttpCallback.onFinish();
                    }
                });
    }


    public void getUserDetails(Context context, final MyHttpCallback<UserDetailResponse> userDetailCallback) {
        MyHttp.getInstance(context, Endpoints.getPatientDetail(SharedPreferencesManager.getStringPreference(SharedPreferencesManager.PATIENT_ID, "")), MyHttp.GET, UserDetailResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .send(new MyHttpCallback<UserDetailResponse>() {
                    @Override
                    public void success(UserDetailResponse data) {
                        if (data.code == null) {
                            setInitialData(data);
                            userDetailCallback.success(data);
                        } else {
                            userDetailCallback.failure(data.message);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        Log.i("Response fail", msg);
                        userDetailCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        userDetailCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        userDetailCallback.onFinish();
                    }
                });
    }

    public void getCaregiverDetails(Context context, final MyHttpCallback<UserDetailResponse> userDetailCallback) {
        MyHttp.getInstance(context, Endpoints.getUserDetail(SharedPreferencesManager.getStringPreference(SharedPreferencesManager.USER_ID, "")), MyHttp.GET, UserDetailResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .send(new MyHttpCallback<UserDetailResponse>() {
                    @Override
                    public void success(UserDetailResponse data) {
                        if (data.code == null) {
                            setInitialData(data);
                            userDetailCallback.success(data);
                        } else {
                            userDetailCallback.failure(data.message);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        Log.i("Response fail", msg);
                        userDetailCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        userDetailCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        userDetailCallback.onFinish();
                    }
                });
    }


    private void setInitialData(UserDetailResponse data) {
        if (data.patientId == null) {
            Hector.rootUserId = "";
            Hector.currentPatientId = "";
        } else {
            Hector.rootUserId = data.patientId;
            Hector.currentPatientId = data.patientId;
        }
        String userName = data.getFullName();
        Hector.rootUserName = userName;
        Hector.currentPatientName = userName;
        Map<String, String> stringPreferenceMap = new HashMap<>();
        stringPreferenceMap.put(SharedPreferencesManager.ROOT_USER_ID, Hector.rootUserId);
        stringPreferenceMap.put(SharedPreferencesManager.ROOT_USER_NAME, userName);
        stringPreferenceMap.put(SharedPreferencesManager.CURRENT_USER_ID, Hector.currentPatientId);
        stringPreferenceMap.put(SharedPreferencesManager.CURRENT_USER_NAME, userName);
        SharedPreferencesManager.setStringPreferenceData(stringPreferenceMap);

    }
}
