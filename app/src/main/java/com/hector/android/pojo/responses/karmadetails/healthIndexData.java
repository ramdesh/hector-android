package com.hector.android.pojo.responses.karmadetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by amal on 18/03/16.
 */
public class healthIndexData {
    @Expose
    @SerializedName("wellness")
    public int wellness;

    @Expose
    @SerializedName("capturedTime")
    public String capturedTime;

    @Expose
    @SerializedName("healthIndex")
    public double healthIndex;

    @Expose
    @SerializedName("trendingValue")
    public double trendingValue;

    @Expose
    @SerializedName("trendingDirection")
    public int trendingDirection;

    @Expose
    @SerializedName("weather")
    public weather Weather;

    @Expose
    @SerializedName("personality")
    public personality Personality;

    @Expose
    @SerializedName("medicationAdherence")
    public medicationAdherence medicationadherence;

}
