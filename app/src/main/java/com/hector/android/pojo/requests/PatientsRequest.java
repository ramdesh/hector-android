package com.hector.android.pojo.requests;

import android.content.Context;

import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.patientdetails.PatientsResponse;

/**
 * Created by arjun on 12/8/15.
 */
public class PatientsRequest {
    public void getMyPatients(Context context, final MyHttpCallback<PatientsResponse> myHttpCallback){
        MyHttp.getInstance(context, Endpoints.getAllAssociatedPatients(SharedPreferencesManager.getStringPreference(SharedPreferencesManager.USER_ID,"")), MyHttp.GET, PatientsResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .send(new MyHttpCallback<PatientsResponse>() {
                    @Override
                    public void success(PatientsResponse data) {
                        if(data.code == null){
                            myHttpCallback.success(data);
                        } else {
                            myHttpCallback.failure(data.message);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        myHttpCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        myHttpCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        myHttpCallback.onFinish();
                    }
                });
    }
}
