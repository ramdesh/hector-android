package com.hector.android.activities;

import android.os.Bundle;

import com.hector.android.R;
import com.hector.android.fragments.IntroFragment;
import com.hector.android.fragments.SignInFragment;

public class LauncherSignInHelper{

    private LauncherActivity context;

    public LauncherSignInHelper(LauncherActivity context) {
        this.context = context;
    }

    public void addFragment(Bundle savedInstanceState){
        if(savedInstanceState == null){
            context.getSupportFragmentManager().beginTransaction()
                    .add(R.id.sign_in_container, new IntroFragment())
                    .commit();
        }
    }
}
