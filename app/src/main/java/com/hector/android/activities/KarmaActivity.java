package com.hector.android.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.hector.android.R;
import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.karmadetails.karmaResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class KarmaActivity extends AppCompatActivity implements View.OnClickListener {

    private String LOG_TAG = "KarmaActivity";
    private double totalKarmaPoints;
    private int karmaPoints = 0;
    private ProgressBar karmaProgress;
    private ScrollView mRootView;
    private LinearLayout karmaDataVisibleView;
    private TextView karmaPointsToDonate, totalKarmaPointsToDonate,
            trending_value, wellness_index, personality_index, adherence_index, weather_index,caregiverKarmaPoint;
    private ImageButton karmaUp, karmaDown;
    private BarChart barChart;
    private Button donate;
    private CardView cardView,caregiver_card_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_karma);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        karmaProgress = (ProgressBar) findViewById(R.id.karma_load_pb);
        mRootView = (ScrollView) findViewById(R.id.karmaRootView);
        karmaDataVisibleView = (LinearLayout) findViewById(R.id.karmaDataVisibleView);
        totalKarmaPointsToDonate = (TextView) findViewById(R.id.totalKarmaPointsToDonate);
        karmaPointsToDonate = (TextView) findViewById(R.id.karmaPointsToDonate);
        trending_value = (TextView) findViewById(R.id.trending_value);
        weather_index = (TextView) findViewById(R.id.weather_index);
        wellness_index = (TextView) findViewById(R.id.wellness_index);
        personality_index = (TextView) findViewById(R.id.personality_index);
        adherence_index = (TextView) findViewById(R.id.adherence_index);
        karmaDown = (ImageButton) findViewById(R.id.karmaDown);
        karmaUp = (ImageButton) findViewById(R.id.karmaUp);
        barChart = (BarChart) findViewById(R.id.barChart);
        donate = (Button) findViewById(R.id.donate_button);
        cardView = (CardView) findViewById(R.id.card_view);
        caregiver_card_view = (CardView) findViewById(R.id.caregiver_card_view);
        caregiverKarmaPoint = (TextView) findViewById(R.id.caregiver_karmapoints);
        donate.setOnClickListener(this);
        karmaUp.setOnClickListener(this);
        karmaDown.setOnClickListener(this);

        getHealthIndex();
    }

    private void getHealthIndex() {

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.WEEK_OF_YEAR, -1);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = formatter.format(calendar.getTime());

        MyHttp.getInstance(Hector.applicationContext, Endpoints.getHealthIndex(formattedDate), MyHttp.GET, karmaResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .send(new MyHttpCallback<karmaResponse>() {
                    @Override
                    public void success(karmaResponse data) {
                        karmaProgress.setVisibility(View.GONE);
                        if (data.code == null) {
                            karmaDataVisibleView.setVisibility(View.VISIBLE);
                            karmaPoints = 0;
                            totalKarmaPoints = data.totalKarma;
                            setUpView(data);

                        } else {

                            String msg = data.message == null || data.message.isEmpty() ? getResources().getString(R.string.http_error) : data.message;
                            Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        karmaProgress.setVisibility(View.GONE);
                        msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                        Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                        Log.i(LOG_TAG, "sending wellness index failed");
                    }

                    @Override
                    public void onBefore() {
                        karmaProgress.setVisibility(View.VISIBLE);
                        karmaDataVisibleView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFinish() {

                    }
                });
    }

    private void setUpView(karmaResponse data) {
        if (SharedPreferencesManager.getStringPreference(SharedPreferencesManager.USER_TYPE, "").equals("caregiver")) {
            cardView.setVisibility(View.GONE);
            caregiverKarmaPoint.setText(String.format("%.2f",data.totalKarma));

        }else {
            caregiver_card_view.setVisibility(View.GONE);
            karmaPointsToDonate.setText(String.valueOf(karmaPoints));
            totalKarmaPointsToDonate.setText(String.format("%.2f", data.totalKarma));
        }
        setData(data);
        if (!data.healthIndexDatas.isEmpty()) {
            trending_value.setText(String.format("%.2f", data.healthIndexDatas.get(0).trendingValue));
            wellness_index.setText(String.format("%.2f", data.healthIndexDatas.get(0).healthIndex));
            weather_index.setText(String.format("%.2f", data.healthIndexDatas.get(0).Weather.weatherIndex));
            personality_index.setText(String.format("%.2f", data.healthIndexDatas.get(0).Personality.personalityIndex));
            adherence_index.setText(String.format("%.2f", data.healthIndexDatas.get(0).medicationadherence.medicationAdherenceIndex));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.karmaDown:
                if (!(karmaPoints <= 0)) {
                    karmaPoints--;
                    karmaPointsToDonate.setText(String.valueOf(karmaPoints));
                }
                break;
            case R.id.karmaUp:
                if (!(karmaPoints >= totalKarmaPoints)) {
                    karmaPoints++;
                    karmaPointsToDonate.setText(String.valueOf(karmaPoints));
                }
                break;
            case R.id.donate_button:
                startActivity(new Intent(this,donationActivity.class));
                break;
        }
    }

    private void setData(karmaResponse data) {

        ArrayList<BarEntry> yVals = new ArrayList<BarEntry>();
        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = data.healthIndexDatas.size() - 1; i >= 0; i--) {
            float val = (float) (Math.round(data.healthIndexDatas.get(i).healthIndex * 100)) / 100;
            yVals.add(new BarEntry((float) val, i));
            xVals.add(Utils.getTimeFormatD(data.healthIndexDatas.get(i).capturedTime));
        }

        BarDataSet set = new BarDataSet(yVals, "Wellness Points");
        set.setColors(new int[]{ColorTemplate.rgb("#ecc104")});
        set.setDrawValues(false);

        barChart.setDescription("");
        barChart.setPinchZoom(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setDrawGridBackground(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(2);


        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinValue(0f);

        Legend l = barChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);


        BarData barData = new BarData(xVals, set);

        barChart.setData(barData);
        barChart.invalidate();
        barChart.animateY(800);
    }
}
