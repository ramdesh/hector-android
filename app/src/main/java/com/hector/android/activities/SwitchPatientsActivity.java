package com.hector.android.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.hector.android.R;
import com.hector.android.application.Hector;
import com.hector.android.application.NetworkStateReceivers;
import com.hector.android.fragments.SwitchPatientFragment;

public class SwitchPatientsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_patients);
        setUpActionBar();
        if (!Hector.isConnectedToInternet) {
            NetworkStateReceivers.showNoConnectivityDialog(SwitchPatientsActivity.this);
        }
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.switch_patient_container, new SwitchPatientFragment())
                    .commit();
        }
    }

    private void setUpActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.switch_patient_toolbar);
        toolbar.setTitle("");
        if (!Hector.currentPatientId.isEmpty())
            toolbar.setNavigationIcon(R.drawable.back_icon_white);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
