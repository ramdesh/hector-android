package com.hector.android.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.hector.android.R;
import com.hector.android.application.DialogClickListener;
import com.hector.android.application.Hector;
import com.hector.android.application.NetworkStateReceivers;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.SettingWithSwitch;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.requests.PreferenceRequest;
import com.hector.android.pojo.requests.Preferences;
import com.hector.android.pojo.responses.PreferencesResponse;

public class PreferenceActivity extends AppCompatActivity {

    private SettingWithSwitch mPushNotificationSetting;
    private SettingWithSwitch mEmailNotificationSetting;
    private ProgressBar mPreferencePb;
    private LinearLayout mRootView;
    private int PUSH = 0;
    private int EMAIL = 1;
    private int SMS = 2;
    private boolean[] currentPreferences = {false, false, false};
    private boolean[] newPreferences = currentPreferences.clone();
    private TypeFaceTextView changePwBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        setUpActionBar();
        if (!Hector.isConnectedToInternet) {
            NetworkStateReceivers.showNoConnectivityDialog(PreferenceActivity.this);
        }
        initialiseViews();
        bindEvents();
        getPreferencesSettings();
    }

    private boolean getPreference(String preferenceKey) {
        return SharedPreferencesManager.getBooleanPreference(preferenceKey, false);
    }

    private void bindEvents() {
        mPushNotificationSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPushNotificationSetting.setChecked(!mPushNotificationSetting.isChecked());
            }
        });
        mPushNotificationSetting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                newPreferences[PUSH] = isChecked;
                if (isChecked) {
                    mPushNotificationSetting.setDetail(getResources().getString(R.string.enabled));
                } else {
                    mPushNotificationSetting.setDetail(getResources().getString(R.string.disabled));
                }
            }
        });
        mEmailNotificationSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEmailNotificationSetting.setChecked(!mEmailNotificationSetting.isChecked());
            }
        });
        mEmailNotificationSetting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                newPreferences[EMAIL] = isChecked;
                if (isChecked) {
                    mEmailNotificationSetting.setDetail(getResources().getString(R.string.enabled));
                } else {
                    mEmailNotificationSetting.setDetail(getResources().getString(R.string.disabled));
                }
            }
        });
//
        changePwBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent changePwIntent = new Intent(PreferenceActivity.this, ChangePasswordActivity.class);
                startActivity(changePwIntent);
            }
        });
    }

    private void initialiseViews() {
        mRootView = (LinearLayout) findViewById(R.id.preferences_root_view);
        mPreferencePb = (ProgressBar) findViewById(R.id.preference_progressBar);
        mPushNotificationSetting = (SettingWithSwitch) findViewById(R.id.push_notification_setting);
        setCurrentState(mPushNotificationSetting, currentPreferences[PUSH]);
        mEmailNotificationSetting = (SettingWithSwitch) findViewById(R.id.email_notification_setting);
        setCurrentState(mEmailNotificationSetting, currentPreferences[EMAIL]);
        changePwBtn = (TypeFaceTextView) findViewById(R.id.change_pw_btn);
    }

    private void setUpViews() {
        setCurrentState(mPushNotificationSetting, currentPreferences[PUSH]);
        setCurrentState(mEmailNotificationSetting, currentPreferences[EMAIL]);
    }

    private void setCurrentState(SettingWithSwitch settingWithSwitch, boolean preference) {
        settingWithSwitch.setChecked(preference);
        settingWithSwitch.setDetail(preference ? getResources().getString(R.string.enabled) : getResources().getString(R.string.disabled));
    }


    private void setUpActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.preference_toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_icon_white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.preference_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.save_preferences:
                if (Hector.isConnectedToInternet) {
                    savePreferences();
                } else {
                    NetworkStateReceivers.showNoConnectivityDialog(PreferenceActivity.this);
                }
        }
        return super.onOptionsItemSelected(item);
    }

    private void getPreferencesSettings() {
        new PreferenceRequest().getPreferenceSettings(PreferenceActivity.this, new MyHttpCallback<PreferencesResponse>() {
            @Override
            public void success(PreferencesResponse data) {
                    currentPreferences[PUSH] = getPreference(SharedPreferencesManager.PUSH_NOTIFICATION_PREFERENCE);
                    currentPreferences[EMAIL] = getPreference(SharedPreferencesManager.EMAIL_NOTIFICATION_PREFERENCE);
                    setUpViews();
            }

            @Override
            public void failure(String msg) {

            }

            @Override
            public void onBefore() {
                Utils.showSnackBar(mRootView, "Fetching Preferences", Snackbar.LENGTH_LONG);
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void savePreferences() {
        if (isPreferenceChanged()) {
            Preferences preferences = new Preferences();
            preferences.push = newPreferences[PUSH];
            preferences.sms = newPreferences[SMS];
            preferences.email = newPreferences[EMAIL];
            new PreferenceRequest().updateNotificationSettings(PreferenceActivity.this, preferences, new MyHttpCallback<PreferencesResponse>() {
                @Override
                public void success(PreferencesResponse data) {
                    currentPreferences = newPreferences.clone();
                    Utils.showSnackBar(mRootView, "Preference Updated", Snackbar.LENGTH_LONG);
                }

                @Override
                public void failure(String msg) {
                    Utils.showSnackBar(mRootView, "Preference Update Failed. Please Try Again.", Snackbar.LENGTH_LONG);
                }

                @Override
                public void onBefore() {
                    mPreferencePb.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFinish() {
                    mPreferencePb.setVisibility(View.GONE);
                }
            });
        } else {
            Utils.showSnackBar(mRootView, "No changes detected", Snackbar.LENGTH_LONG);
        }
    }

    private boolean isPreferenceChanged() {
        int index = 0;
        for (boolean preference : currentPreferences) {
            if (preference != newPreferences[index]) {
                return true;
            }
            index++;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (isPreferenceChanged()) {
            showUnsavedAlert();
        } else {
            super.onBackPressed();
        }
    }

    private void showUnsavedAlert() {
        Utils.showAlertDialog(PreferenceActivity.this, getResources().getString(R.string.are_you_sure),
                getResources().getString(R.string.not_saved_detail), false, getResources().getString(R.string.continue_task),
                getResources().getString(R.string.cancel), new DialogClickListener() {
                    @Override
                    public void onPositiveButtonClicked(AlertDialog alertDialog) {
                        alertDialog.dismiss();
                        finish();
                    }

                    @Override
                    public void onNegativeButtonClicked(AlertDialog alertDialog) {
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onDialogDismissed(AlertDialog alertDialog) {
                    }
                });
    }

}
