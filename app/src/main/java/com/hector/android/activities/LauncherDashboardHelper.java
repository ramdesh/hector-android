package com.hector.android.activities;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.PointTarget;
import com.hector.android.R;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.fragments.DashBoardFragment;
import com.hector.android.fragments.DatePickerDialogFragment;
import com.hector.android.fragments.SideNavigationDrawer;
import com.hector.android.libs.MyPagerAdapter;
import com.hector.android.libs.SlidingTabLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by arjun on 11/25/15.
 */
public class LauncherDashboardHelper implements DatePickerDialogFragment.DateSetListener {
    public static final String DATE_TO_FETCH = "DATE_TO_FETCH";
    public static final String DO_REGISTER_RECEIVER = "DO_REGISTER_RECEIVER";
    public static final String DATE_CHANGED = "DATE_CHANGED";
    private static final int LAST_TAB_INDEX = 7;
    private static final String MONTH_TO_FETCH = "MONTH_TO_FETCH";
    private static final String YEAR_TO_FETCH = "YEAR_TO_FETCH";
    public ScrollListener scrollListener = new ScrollListener() {
        @Override
        public void onScrolledUp() {
            Log.i("Scrolled up", "Scrolled up");
        }

        @Override
        public void onScrolledDown() {
            Log.i("Scrolleac down", "Scrolled down");
        }
    };
    private LauncherActivity context;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private String DATE_STRING_FORMAT = "%02d";
    private DashBoardFragment dashBoardFragment;
    private ViewPager mViewPager;
    private SchedulesPagerAdapter mPagerAdapter;
    private SlidingTabLayout mSlidingTabLayout;
    private String[] days = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT", ""};
    private int dayOfWeek;
    private ArrayList<String> datesOfWeek;
    private int interpolatedMarginVal;
    private AppBarLayout toolbarLayout;
    private int tabsCount;
    private boolean tabAlreadyCreated = false;
    private ArrayList<Object> monthOfDatesOfWeek;
    private ArrayList<Object> yearOfDatesOfWeek;
    private ShowcaseView hamburgerShowcaseView, introShowcaseView;
    private boolean isActionVisible = true;

    public LauncherDashboardHelper(final LauncherActivity context) {
        this.context = context;

        if (!SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.intro_showcase, false)) {
            final int width = context.getWindowManager().getDefaultDisplay().getWidth();
            final int height = context.getWindowManager().getDefaultDisplay().getHeight();
            final Handler introHandler = new Handler();
            introHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    introShowcaseView = new ShowcaseView.Builder(context)
                            .withMaterialShowcase()
                            .setTarget(new PointTarget(width + 100, height + 100))
                            .setContentText("Let's walk you through the app." + System.getProperty("line.separator") + "Please follow the interactive tour, for easy understanding.")
                            .setStyle(R.style.CustomShowcaseTheme2)
                            .replaceEndButton(R.layout.view_custom_button_normal)
                            .setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    introShowcaseView.hide();
                                    SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.intro_showcase, true);
                                    if (!SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.hamburger_Showcase, false)) {
                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                LayoutInflater inflater = (LayoutInflater) context.getSystemService
                                                        (Context.LAYOUT_INFLATER_SERVICE);
                                                View view = inflater.inflate(R.layout.view_custom_button_normal, null);
                                                Button button = (Button) view;
                                                button.setText("Click it");

                                                hamburgerShowcaseView = new ShowcaseView.Builder(context)
                                                        .withMaterialShowcase()
                                                        .setTarget(new PointTarget(35, 75))
                                                        .setContentTitle("This Hamburger Icon")
                                                        .setContentText("Reveal's a menu, that can take you to any part of the app.")
                                                        .setStyle(R.style.CustomShowcaseTheme2)
                                                        .replaceEndButton(button)
                                                        .setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                hamburgerShowcaseView.hide();
                                                                SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.hamburger_Showcase, true);
                                                                mDrawerLayout.openDrawer(Gravity.LEFT);
                                                            }
                                                        })
                                                        .build();
                                            }
                                        }, 500);

                                    }
                                }
                            })
                            .build();
                }
            }, 800);
        }
    }

    public void setUpActionBar() {
        Toolbar toolbar = (Toolbar) context.findViewById(R.id.dashboard_toolbar);
        toolbar.setTitle("");
        context.setSupportActionBar(toolbar);
        context.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
    }

    public void setUpNavDrawer(Bundle savedInstanceState) {
        Fragment fragment;
        if (savedInstanceState == null) {
            FragmentManager frag = context.getSupportFragmentManager();
            fragment = new SideNavigationDrawer();
            frag.beginTransaction().add(R.id.left_drawer, fragment).commit();
        } else {
            fragment = context.getSupportFragmentManager().findFragmentById(R.id.left_drawer);
        }
        mDrawerLayout = (DrawerLayout) context.findViewById(R.id.drawer_layout);
        ((SideNavigationDrawer) fragment).drawerLayout = mDrawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(context, mDrawerLayout, null, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                context.invalidateOptionsMenu();
                // hide hamburger showcase
                if (hamburgerShowcaseView != null)
                    if (hamburgerShowcaseView.isShowing()) {
                        hamburgerShowcaseView.hide();
                        SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.hamburger_Showcase, true);
                    }
                // broadcast intent to SideNavigationDrawer to show device config showcase
                Intent intent = new Intent("device-configuration-onboarding");
                if (SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.hamburger_Showcase, false) &&
                        !SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.deviceConfig_showcase, false)) {
                    intent.putExtra("subject", context.getResources().getString(R.string.title_activity_device_configuration));
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                }
                if (SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.hamburger_Showcase, false) &&
                        SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.deviceConfig_showcase, false) &&
                        !SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.activities_showcase, false)) {
                    intent.putExtra("subject", context.getResources().getString(R.string.activities));
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                }

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                context.invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    public boolean isDrawerStateChanged(MenuItem item) {
        return mDrawerToggle.isDrawerIndicatorEnabled() &&
                mDrawerToggle.onOptionsItemSelected(item);
    }

    public void initialiseViews() {
        toolbarLayout = (AppBarLayout) context.findViewById(R.id.dashboard_toolbar_layout);
        computeValues();
        mViewPager = (ViewPager) context.findViewById(R.id.schedules_pager);
        mPagerAdapter = new SchedulesPagerAdapter(context.getSupportFragmentManager());
        tabsCount = 7;
        mViewPager.setAdapter(mPagerAdapter);
        mSlidingTabLayout = (SlidingTabLayout) context.findViewById(R.id.date_sliding_tab_layout);
        mSlidingTabLayout.setCustomTabView(R.layout.custom_tab_view_dashboard, R.id.custom_tab_tv_dashboard);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return context.getResources().getColor(R.color.colorAccent);
            }
        });
        mSlidingTabLayout.setViewPager(mViewPager);
        mViewPager.setCurrentItem(dayOfWeek, true);
        bindEvents();
    }

    private void bindEvents() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (!isActionVisible) {
                    isActionVisible = true;
                    startShowAnimation();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void computeValues() {
        Calendar cal = Calendar.getInstance();
        int todayDate = cal.get(Calendar.DATE);
        dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DATE,todayDate-dayOfWeek);
        datesOfWeek = new ArrayList<>();
        monthOfDatesOfWeek = new ArrayList<>();
        yearOfDatesOfWeek = new ArrayList<>();
        int firstDayOfWeek = cal.get(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < 7; i++) {
            Log.i("Dated ", String.valueOf(firstDayOfWeek));
            datesOfWeek.add(String.format(DATE_STRING_FORMAT, cal.get(Calendar.DAY_OF_MONTH)));
            monthOfDatesOfWeek.add(String.format(DATE_STRING_FORMAT, cal.get(Calendar.MONTH) + 1));
            yearOfDatesOfWeek.add(String.valueOf(cal.get(Calendar.YEAR)));
            cal.add(Calendar.DATE, 1);
        }
        datesOfWeek.add("");
        monthOfDatesOfWeek.add("");
        yearOfDatesOfWeek.add("");
    }

    private void startShowAnimation() {
        final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(-interpolatedMarginVal, 0);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int val = (int) animation.getAnimatedValue();
                layoutParams.setMargins(0, val, 0, 0);
                toolbarLayout.setLayoutParams(layoutParams);
            }
        });
        valueAnimator.setDuration(300);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.start();
    }

    private void startHideAnimation() {
        final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(0, toolbarLayout.getHeight());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                interpolatedMarginVal = (int) animation.getAnimatedValue();
                layoutParams.setMargins(0, -interpolatedMarginVal, 0, 0);
                toolbarLayout.setLayoutParams(layoutParams);
            }
        });
        valueAnimator.setDuration(600);
        valueAnimator.setInterpolator(new AccelerateInterpolator(2));
        valueAnimator.start();
    }

    @Override
    public void onDateSet(int year, int month, int day) {
        Log.i("Date ", year + " " + month + " " + day);
        int index = 0;
        for (String date : datesOfWeek) {
            if (date.equals(String.valueOf(day))) {
                if (monthOfDatesOfWeek.get(index).equals(String.valueOf(month))
                        && yearOfDatesOfWeek.get(index).equals(String.valueOf(year))) {
                    mViewPager.setCurrentItem(index);
                    return;
                }
            }
            index++;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day);
        String selectedDate = new SimpleDateFormat("MMM\ndd").format(calendar.getTime());
        SpannableString spannableString = new SpannableString(selectedDate.toUpperCase());
        spannableString.setSpan(new RelativeSizeSpan(1.0f), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        datesOfWeek.remove(LAST_TAB_INDEX);
        datesOfWeek.add(LAST_TAB_INDEX, String.format(DATE_STRING_FORMAT, day));
        monthOfDatesOfWeek.remove(LAST_TAB_INDEX);
        monthOfDatesOfWeek.add(LAST_TAB_INDEX, String.format(DATE_STRING_FORMAT, month));
        yearOfDatesOfWeek.remove(LAST_TAB_INDEX);
        yearOfDatesOfWeek.add(LAST_TAB_INDEX, String.valueOf(year));
        if (!tabAlreadyCreated) {
            tabsCount = 8;
            mPagerAdapter.notifyDataSetChanged();
            mSlidingTabLayout.addTab(spannableString, "");
            tabAlreadyCreated = true;
        } else {
            mSlidingTabLayout.setTabTitle(LAST_TAB_INDEX, spannableString);
            Intent newDateIntent = new Intent(DATE_CHANGED);
            newDateIntent.putExtra(DATE_TO_FETCH,
                    yearOfDatesOfWeek.get(LAST_TAB_INDEX)
                            + "-" + monthOfDatesOfWeek.get(LAST_TAB_INDEX)
                            + "-" + datesOfWeek.get(LAST_TAB_INDEX));
            LocalBroadcastManager.getInstance(context).sendBroadcast(newDateIntent);
        }
        mViewPager.setCurrentItem(LAST_TAB_INDEX);
    }


    public interface ScrollListener {
        void onScrolledUp();

        void onScrolledDown();
    }

    public class SchedulesPagerAdapter extends MyPagerAdapter {

        public SchedulesPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getTabTextColor() {

            return R.color.tab_text_color_dashboard;
        }

        @Override
        public Fragment getItem(int position) {
            DashBoardFragment fragmentInstance = DashBoardFragment.getFragmentInstance(scrollListener);
            Bundle bundle = new Bundle();
            bundle.putString(DATE_TO_FETCH,
                    yearOfDatesOfWeek.get(position) + "-" + monthOfDatesOfWeek.get(position) + "-" + datesOfWeek.get(position));
            fragmentInstance.setArguments(bundle);
            bundle.putBoolean(DO_REGISTER_RECEIVER, position == LAST_TAB_INDEX);
            return fragmentInstance;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);
            String title = days[position];
            String date = datesOfWeek.get(position);
            if (date != null && !date.isEmpty()) {
                title = title + "\n" + date;
                SpannableString titleString = new SpannableString(title);
                if (dayOfWeek==position){
                    titleString.setSpan(new ForegroundColorSpan(0xffF7F36F), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                titleString.setSpan(styleSpan, title.length() - date.length(), title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                titleString.setSpan(new RelativeSizeSpan(1.0f), title.length() - date.length(), title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                return titleString;
            }
            return title;
        }

        @Override
        public int getCount() {
            return tabsCount;
        }
    }
}
