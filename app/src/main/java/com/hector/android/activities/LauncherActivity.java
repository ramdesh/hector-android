package com.hector.android.activities;

import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.PointTarget;
import com.hector.android.R;
import com.hector.android.application.Hector;
import com.hector.android.application.NetworkStateReceivers;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.fragments.DatePickerDialogFragment;
import com.hector.android.gcm.GcmRegistration;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.ErrorHttpResponse;
import com.hector.android.ui.SeekArc;
import com.hector.android.widget.appWidgetProvider;
import com.splunk.mint.Mint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class LauncherActivity extends AppCompatActivity {

    private static final String LOG_TAG = "LauncherActivity";

    private LauncherDashboardHelper launcherDashboardHelper;
    private NetworkStateReceivers networkStateReceiver;
    private ShowcaseView scheduleShowcaseView;
    private BroadcastReceiver alarmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String reminderMsg = SharedPreferencesManager.getStringPreference(SharedPreferencesManager.REMINDER, "");
            alarmAlertBox(reminderMsg);
        }
    };

    private SeekArc mSeekArc;
    private TextView mSeekArcProgress, feelingTodayText;
    private Button submitFeelingIndex;
    private ImageView smiley_image;
    private int rating;
    private int[] smiley = {R.drawable.one, R.drawable.two, R.drawable.three, R.drawable.four, R.drawable.five, R.drawable.six,
            R.drawable.seven, R.drawable.eight, R.drawable.nine, R.drawable.ten};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mint.initAndStartSession(LauncherActivity.this, "00aee34e");
        NetworkStateReceivers.resetNetworkState(Hector.applicationContext);
        if (!Hector.isConnectedToInternet) {
            NetworkStateReceivers.showNoConnectivityDialog(LauncherActivity.this);
        }
        registerNetWorkReceiver();
        registerAlarmReceiver();

        if (Hector.isUserLoggedIn()) {

            if (SharedPreferencesManager.getStringPreference(SharedPreferencesManager.USER_TYPE, "").equals("caregiver")) {
                if (Hector.currentPatientId == "") {
                    Intent switchPatientIntent = new Intent(this, SwitchPatientsActivity.class);
                    switchPatientIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(switchPatientIntent);
                    finish();
                    return;
                }
            }

            setContentView(R.layout.activity_dashboard);
            launcherDashboardHelper = new LauncherDashboardHelper(LauncherActivity.this);
            launcherDashboardHelper.setUpActionBar();
            launcherDashboardHelper.setUpNavDrawer(savedInstanceState);
            launcherDashboardHelper.initialiseViews();
            GcmRegistration.getInstance(LauncherActivity.this).getAndSendGcmRegId();
            // to show alert
            String reminderMsg = SharedPreferencesManager.getStringPreference(SharedPreferencesManager.REMINDER, "");
            if (!reminderMsg.equals("")) {
                alarmAlertBox(reminderMsg);
            } else {
                if (!SharedPreferencesManager.getStringPreference(SharedPreferencesManager.USER_TYPE, "").equals("caregiver")) {
                    if (SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.activities_showcase, false) == true) {
                        Calendar calendar = Calendar.getInstance();
                        if (SharedPreferencesManager.getStringPreference(SharedPreferencesManager.FEELING_INDEX_NEXT_TIME, "").equals("")) {
                            int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                            if (currentHour >= 6 && currentHour <= 19) {
                                showFeelingIndex();
                            }
                        } else {
                            String lastTime = SharedPreferencesManager.getStringPreference(SharedPreferencesManager.FEELING_INDEX_NEXT_TIME, "");
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                Date lastDate = formatter.parse(lastTime);
                                Calendar calendar1 = Calendar.getInstance();
                                Date currentDate = formatter.parse(formatter.format(calendar1.getTime()));
                                if (lastDate.compareTo(currentDate) == -1) {
                                    int currentHour = calendar1.get(Calendar.HOUR_OF_DAY);
                                    if (currentHour >= 6 && currentHour <= 19) {
                                        showFeelingIndex();
                                    }
                                }

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }


        } else {
            setContentView(R.layout.activity_sign_in);
            LauncherSignInHelper launcherSignInHelper = new LauncherSignInHelper(LauncherActivity.this);
            launcherSignInHelper.addFragment(savedInstanceState);
        }

    }

    private void updateWidget() {
        Intent intent = new Intent(this, appWidgetProvider.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), appWidgetProvider.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        sendBroadcast(intent);
    }

    private void showFeelingIndex() {
        rating = 1;
        AlertDialog.Builder feelingsBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.feeling_index, null);
        feelingsBuilder.setView(dialogView);

        mSeekArc = (SeekArc) dialogView.findViewById(R.id.seekArc);
        mSeekArcProgress = (TextView) dialogView.findViewById(R.id.seekArcProgress);
        feelingTodayText = (TextView) dialogView.findViewById(R.id.feelingTodayText);
        submitFeelingIndex = (Button) dialogView.findViewById(R.id.submitFeelingIndex);
        smiley_image = (ImageView) dialogView.findViewById(R.id.smiley);

        feelingTodayText.setText("Hello " + Hector.currentPatientName + ", how are you feeling today?");


        mSeekArc.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekArc seekArc) {
            }

            @Override
            public void onStartTrackingTouch(SeekArc seekArc) {
            }

            @Override
            public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
                mSeekArcProgress.setText(String.valueOf(progress + 1));
                rating = progress + 1;
                smiley_image.setImageResource(smiley[progress]);
            }
        });

        final AlertDialog feelingsDialog = feelingsBuilder.create();
        submitFeelingIndex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFeelingIndexToServer();
                feelingsDialog.dismiss();
            }
        });


        feelingsDialog.show();
    }

    private void sendFeelingIndexToServer() {
        Map<String, String> gmcIdMap = new HashMap<>();
        gmcIdMap.put("deviceToken", "");
        gmcIdMap.put("deviceType", "android");
        MyHttp.getInstance(Hector.applicationContext, Endpoints.getWellnessIndexUrl(String.valueOf(rating)), MyHttp.POST, ErrorHttpResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .addJson(gmcIdMap)
                .send(new MyHttpCallback<ErrorHttpResponse>() {
                    @Override
                    public void success(ErrorHttpResponse data) {

                        if (data.code == null) {
                            Calendar calendar = Calendar.getInstance();
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                            String formattedDate = formatter.format(calendar.getTime());
                            SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.FEELING_INDEX_NEXT_TIME, formattedDate);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        Log.i(LOG_TAG, "sending wellness index failed");
                    }

                    @Override
                    public void onBefore() {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateWidget();
        if (launcherDashboardHelper != null) {
            if (SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.wifiConfig_showcase, false) &&
                    !SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.schedule_showcase, false)) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        LayoutInflater inflater = (LayoutInflater) getSystemService
                                (Context.LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.view_custom_button_normal, null);
                        Button button = (Button) view;
                        button.setText("Cool");

                        scheduleShowcaseView = new ShowcaseView.Builder(LauncherActivity.this)
                                .withMaterialShowcase()
                                .setTarget(new PointTarget(100, 250))
                                .setContentText("This is where you can check the medication schedule." +
                                        System.getProperty("line.separator") +
                                        "ie, when what needs to be taken.")
                                .setStyle(R.style.CustomShowcaseTheme2)
                                .replaceEndButton(button)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.schedule_showcase, true);
                                        scheduleShowcaseView.hide();
                                    }
                                })
                                .build();
                    }
                }, 500);
            }
        }

    }

    private void registerNetWorkReceiver() {
        IntentFilter networkStateFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        networkStateReceiver = new NetworkStateReceivers();
        registerReceiver(networkStateReceiver, networkStateFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(LOG_TAG, "OnDestroy");
        unregisterReceiver(networkStateReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (launcherDashboardHelper != null && launcherDashboardHelper.isDrawerStateChanged(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.date_picker:
                if (launcherDashboardHelper != null) {
                    DatePickerDialogFragment datePickerDialogFragment = new DatePickerDialogFragment();
                    datePickerDialogFragment.setOnDatePickerListener(launcherDashboardHelper);
                    datePickerDialogFragment.show(getSupportFragmentManager(), "DatePicker");
                }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.launcher_menu, menu);

        return true;
    }


    private void registerAlarmReceiver() {
        LocalBroadcastManager.getInstance(this).registerReceiver(alarmReceiver,
                new IntentFilter("alarmReceiver"));
    }

    private void unRegisterAlarmReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(alarmReceiver);
    }

    private void alarmAlertBox(String reminderMsg) {

        if (!isFinishing()) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.reminder_layout, null);
            dialogBuilder.setView(dialogView);
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.setCancelable(false);
            TextView reminderTextView = (TextView) dialogView.findViewById(R.id.ReminderMsg);
            reminderTextView.setText(reminderMsg);
            Button close = (Button) dialogView.findViewById(R.id.reminderClose);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.REMINDER, "");

                    if (com.hector.android.services.alarmReceiver.mp != null)
                        com.hector.android.services.alarmReceiver.mp.stop();
                }
            });
            alertDialog.show();
        }
    }

}
