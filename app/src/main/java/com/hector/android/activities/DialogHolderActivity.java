package com.hector.android.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;

import com.hector.android.R;
import com.hector.android.application.DialogClickListener;
import com.hector.android.application.Utils;
import com.hector.android.libs.TypeFaceTextView;

public class DialogHolderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activities_dialog_holder);
        showNoConnectivityDialog();
    }


    private void showNoConnectivityDialog(){
        Utils.showAlertDialog(DialogHolderActivity.this, getResources().getString(R.string.oops),
                getResources().getString(R.string.check_network), true, getResources().getString(R.string.try_again),
                "", new DialogClickListener() {
                    @Override
                    public void onPositiveButtonClicked(AlertDialog alertDialog) {
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onNegativeButtonClicked(AlertDialog alertDialog) {

                    }

                    @Override
                    public void onDialogDismissed(AlertDialog alertDialog) {
                        finish();
                    }
                });
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
