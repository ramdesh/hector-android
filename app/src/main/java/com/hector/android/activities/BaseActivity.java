package com.hector.android.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import com.hector.android.application.Hector;
import com.hector.android.application.NetworkStateReceivers;

/**
 * Created by arjun on 12/7/15.
 */
public class BaseActivity extends AppCompatActivity {
    private BroadcastReceiver networkStateReceiver;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!Hector.isConnectedToInternet){
            NetworkStateReceivers.showNoConnectivityDialog(BaseActivity.this);
        }
        if(!Hector.isConnectedToInternet){
            showNoConnectivityDialog();
        }
        registerNetWorkStateReceiver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(BaseActivity.this).unregisterReceiver(networkStateReceiver);
    }

    private void registerNetWorkStateReceiver() {
        networkStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(Hector.isConnectedToInternet){
                    restartActivity();
                } else {
                    if(alertDialog != null && alertDialog.isShowing()) {
                        return;
                    }
                    showNoConnectivityDialog();
                }
            }
        };
        LocalBroadcastManager.getInstance(BaseActivity.this).registerReceiver(networkStateReceiver, new IntentFilter(NetworkStateReceivers.NETWORK_CONNECTION_STATE_CHANGED));
    }


    public void showNoConnectivityDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
        builder.setTitle("Network Disconnected");
        builder.setMessage("Please connect to internet and try again");
        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Hector.isConnectedToInternet == false) {
                    return;
                } else {
                    restartActivity();
                    dialog.dismiss();
                }
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    private void restartActivity() {
        onCreate(new Bundle());
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(BaseActivity.this).unregisterReceiver(networkStateReceiver);
    }
}
