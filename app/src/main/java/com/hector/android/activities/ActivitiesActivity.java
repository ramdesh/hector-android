package com.hector.android.activities;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;

import com.hector.android.R;
import com.hector.android.application.Hector;
import com.hector.android.application.NetworkStateReceivers;
import com.hector.android.fragments.ActivitiesFragment;
import com.hector.android.libs.MyPagerAdapter;
import com.hector.android.libs.SlidingTabLayout;

public class ActivitiesActivity extends AppCompatActivity {
    public static final int SCROLL_UP_THRESHOLD = 10;
    public static final int SCROLL_DOWN_THRESHOLD = -10;
    public static final String ACTIVITY_TYPE_ALL = "";
    public static final String ACTIVITY_TYPE_INFO = "info";
    public static final String ACTIVITY_TYPE_WARNING = "warning";
    public static final String ACTIVITY_TYPE_CRITICAL = "critical";
    private static final int ACTIVITY_TYPES_COUNT = 2;
    private String[] activityTypes = {ACTIVITY_TYPE_ALL, ACTIVITY_TYPE_CRITICAL, ACTIVITY_TYPE_WARNING};
    private String[] activityTypeTitles = {"All", "Critical", "Warning"};

    /**
     * field to know whether activities page is opened and messages will be
     * consumed
     */
    public static boolean isScreenLive = false;

    private ViewPager mViewPager;
    private AppBarLayout toolbarLayout;
    private int interpolatedMarginVal;
    private RelativeLayout mRootView;

    public enum fragmentsEnum {
        all,

        critical,

        warning
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activities);
        String receivedId = getIntent().getStringExtra("userId");
        String receivedName = getIntent().getStringExtra("userName");
        if(receivedId != null && receivedName != null){
            Hector.currentPatientId = receivedId;
            Hector.currentPatientName = receivedName;
        }
        setUpActionBar();
        initialiseViews();
        bindEvents();
        if(!Hector.isConnectedToInternet){
            NetworkStateReceivers.showNoConnectivityDialog(ActivitiesActivity.this);
        }
    }

    private void bindEvents() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(!isActionVisible) {
                    isActionVisible = true;
                    startShowAnimation();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isScreenLive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isScreenLive = false;
    }

    private void initialiseViews() {
        mRootView = (RelativeLayout) findViewById(R.id.activities_root_view);
        mViewPager = (ViewPager) findViewById(R.id.activities_view_pager);
        mViewPager.setOffscreenPageLimit(2);
        ActivitiesPagerAdapter mPagerAdapter = new ActivitiesPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.activities_sliding_tab_layout);
        mSlidingTabLayout.setCustomTabView(R.layout.custom_tab_view, R.id.custom_tab_tv);
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }
        });
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    private void setUpActionBar() {
        toolbarLayout = (AppBarLayout) findViewById(R.id.activities_toolbar_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.activities_toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_icon_white));
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isActionVisible = true;
    public ScrollListener scrollListener = new ScrollListener() {
        @Override
        public void onScrolledUp() {
            Log.i("Scrolled up", "Scrolled up");
            if(isActionVisible) {
                startHideAnimation();
                isActionVisible = false;
            }
        }

        @Override
        public void onScrolledDown() {
            Log.i("Scrolled down", "Scrolled down");
            if(!isActionVisible) {
                startShowAnimation();
                isActionVisible = true;
            }
        }
    };

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    private void startShowAnimation() {
        final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(-interpolatedMarginVal, 0);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int val = (int) animation.getAnimatedValue();
                layoutParams.setMargins(0, val, 0 , 0);
                toolbarLayout.setLayoutParams(layoutParams);
            }
        });
        valueAnimator.setDuration(300);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.start();
    }

    private void startHideAnimation() {
        final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(0, toolbarLayout.getHeight());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                interpolatedMarginVal = (int) animation.getAnimatedValue();
                layoutParams.setMargins(0, -interpolatedMarginVal, 0 , 0);
                toolbarLayout.setLayoutParams(layoutParams);
            }
        });
        valueAnimator.setDuration(600);
        valueAnimator.setInterpolator(new AccelerateInterpolator(2));
        valueAnimator.start();
    }

    private ActivitiesFragment.MessageListener messageListener = new ActivitiesFragment.MessageListener() {
        @Override
        public void messageReceived(final ActivitiesActivity.fragmentsEnum position){
            Snackbar.make(mRootView, "New activities received", Snackbar.LENGTH_LONG)
                    .setAction("Show", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(position.ordinal(), true);
                        }
                    })
                    .setActionTextColor(getResources().getColor(R.color.white))
                    .show();
        }
    };

    private class ActivitiesPagerAdapter extends MyPagerAdapter{

        public ActivitiesPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getTabTextColor() {
            return R.color.tab_text_colors;
        }

        @Override
        public Fragment getItem(int position) {
            return ActivitiesFragment
                    .getFragmentInstance(scrollListener, messageListener, activityTypes[position]);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return activityTypeTitles[position];
        }

        @Override
        public int getCount() {
            return ACTIVITY_TYPES_COUNT;
        }
    }

    public interface ScrollListener{
        void onScrolledUp();
        void onScrolledDown();
    }
}
